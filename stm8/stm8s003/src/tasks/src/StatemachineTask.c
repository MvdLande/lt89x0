#include "stm8s.h"
#include "common.h"


#define WALK_TIME (10000 / TICKPERIOD) /*Walk signal is 10 seconds WAIT */
#define TRANSITION_TIME (1000 / TICKPERIOD) 
#define AUTOWAIT_TIME (10000 / TICKPERIOD) 
//#define WALK_TIME 10 /*Walk signal is 10 seconds WAIT */
//#define TRANSITION_TIME 4 


static uint8_t update_state(uint8_t display_state);
static void update_leds(uint8_t display_state);

enum DISPLAY_STATE
{
	PEDESTRIAN_RED, /* car_green */
	CAR_YELLOW, /* pedestrian_red */
	CAR_RED,  /*pedestrian_red */ 
	PEDESTRIAN_GREEN, /* CAR_RED */
	LEDS_OFF
};

enum OPERATION_MODE
{
	WAIT,
	WALK_REQUEST,
	WALK_REQUEST_END,
	BLINK,
	GO_REQUEST,
	STOP_REQUEST,
	AUTO_REQUEST,
	AUTO_REQUEST_END,
	AUTO_REQUEST_WAIT,
	INIT
};

void statemachine_task(void)
{
	static uint8_t display_state = PEDESTRIAN_RED;
	uint8_t temp;
	temp = scheduler_getVar(INIT_HANDLE);
	if (INIT_STATEMACHINETASK == (temp & INIT_STATEMACHINETASK))
	{
		display_state = PEDESTRIAN_RED;
		scheduler_setVar(INIT_HANDLE, temp & ~INIT_STATEMACHINETASK);
		display_state = update_state(INIT);
	}
	else
	{
		display_state = update_state(display_state);
	}
	update_leds(display_state);
}

static uint8_t update_state(uint8_t current_state)
{
	//static uint8_t mode = WAIT; // wait for request
	static uint8_t mode = AUTO_REQUEST; //automatic mode
	static uint16_t timestamp;
	uint8_t flags;
	uint8_t display_state = PEDESTRIAN_RED;
	if (INIT == current_state)
	{
		display_state = PEDESTRIAN_RED;
		mode = AUTO_REQUEST; //automatic mode
		//mode = BLINK;
		timestamp = scheduler_getTime();		
	}
	else
	{
		display_state = current_state;		
	}
	flags = scheduler_getVar(FLAGS_HANDLE);

	// *******************************************************
	// process flags
	// *******************************************************
	// WALK_FLAG
	// *******************************************************
	if((flags & WALK_FLAG) !=0)
	{
		flags = flags & ~WALK_FLAG; // clear flag	
		scheduler_setVar(FLAGS_HANDLE, flags);
		// check current mode
		if ( (WALK_REQUEST == mode) || (WALK_REQUEST_END == mode) )
		{
			// do nothing
		}
		else
		{
			// update mode
			mode = WALK_REQUEST;
			timestamp = scheduler_getTime();
			// update display_state
			display_state = PEDESTRIAN_RED;
		}
	}
	// *******************************************************
	// BLINK_FLAG
	// *******************************************************
	if((flags & BLINK_FLAG) != 0)
	{
		flags = flags & ~BLINK_FLAG; // clear flag
		scheduler_setVar(FLAGS_HANDLE, flags);
		// check current mode		
		if (BLINK == mode)
		{
			// do nothing
		}
		else
		{
			// update mode
			mode = BLINK; // enter BLINK mode
			timestamp = scheduler_getTime();
			// update display_state
			display_state  = CAR_YELLOW;
		}
	}
	// *******************************************************
	// GO_FLAG
	// *******************************************************
	if((flags & GO_FLAG) !=0)
	{
		flags = flags & ~GO_FLAG; // clear flag
		scheduler_setVar(FLAGS_HANDLE, flags);
		// update mode
		mode = WAIT; // Keep this output untill another request is received
		timestamp = scheduler_getTime();
		// update display_state
		display_state = PEDESTRIAN_RED;
	}
	// *******************************************************
	// STOP_FLAG
	// *******************************************************
	if((flags & STOP_FLAG) != 0)
	{
		flags = flags & ~STOP_FLAG; // clear flag
		scheduler_setVar(FLAGS_HANDLE, flags);
		// update mode
		mode = WAIT; // Keep this output untill another request is received
		timestamp = scheduler_getTime();
		// update display_state
		display_state = PEDESTRIAN_GREEN;
	}
	// *******************************************************
	// AUTO_FLAG
	// *******************************************************
	if((flags & AUTO_FLAG) != 0)
	{
		flags = flags & ~AUTO_FLAG; // clear flag
		scheduler_setVar(FLAGS_HANDLE, flags);
		// check current mode
		if ( (AUTO_REQUEST == mode) || (AUTO_REQUEST_END == mode) )
		{
			// do nothing
		}
		else
		{
			// update mode
			mode = AUTO_REQUEST; // no function assigned yet.
			timestamp = scheduler_getTime();
			// update display_state
			display_state = PEDESTRIAN_RED;
		}
	}	
	
	// *******************************************************
	// process modes, modify display_state
	// *******************************************************
	// *******************************************************
	// BLINK mode
	// *******************************************************	
	switch (mode)
	{
		case BLINK:
			if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
			{
				if (CAR_YELLOW != display_state){
					display_state = CAR_YELLOW;
					timestamp = scheduler_getTime();
				}
				else
				{
					display_state = LEDS_OFF;
					timestamp = scheduler_getTime();
				}
			}
			break;	
	// *******************************************************
	// WALK_REQUEST or AUTO_REQUEST mode
	// *******************************************************	
		case WALK_REQUEST:
		case AUTO_REQUEST:
			switch (display_state)
			{
				case PEDESTRIAN_RED:
					if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
					{
						display_state = CAR_YELLOW;
						timestamp = scheduler_getTime();
					}
					break;
				case CAR_YELLOW:
					if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
					{
						display_state = CAR_RED;
						timestamp = scheduler_getTime();
					}
					break;
				case CAR_RED:
					if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
					{
						display_state = PEDESTRIAN_GREEN;
						timestamp = scheduler_getTime();
					}
					break;
				case PEDESTRIAN_GREEN:
				default:
					if(scheduler_getTimeElapsed(timestamp) > WALK_TIME)
					{
						display_state = CAR_YELLOW;
						timestamp = scheduler_getTime();
						if(WALK_REQUEST == mode)
						{
							mode = WALK_REQUEST_END;
						}
						else
						{
							mode = AUTO_REQUEST_END;
						}
					}
			}
			break;	
	// *******************************************************
	// WALK_REQUEST_END or AUTO_REQUEST_END mode
	// *******************************************************	
		case WALK_REQUEST_END:
		case AUTO_REQUEST_END:
			switch(display_state)
			{
				case PEDESTRIAN_GREEN:
					if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
					{
						display_state = CAR_RED;
						timestamp = scheduler_getTime();
					}
					break;

				case CAR_RED:
					if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
					{
						display_state = CAR_YELLOW;
						timestamp = scheduler_getTime();
					}
					break;

				case CAR_YELLOW:
					if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
					{
						display_state = PEDESTRIAN_RED;
						timestamp = scheduler_getTime();
						if(WALK_REQUEST_END == mode)
						{
							mode = WAIT;
						}
						else
						{
							mode = AUTO_REQUEST_WAIT;					
						}
					}
					break;
				default:
					display_state = CAR_YELLOW;
					timestamp = scheduler_getTime();
			}
			break;				
	// *******************************************************
	// AUTO_REQUEST_WAIT mode
	// *******************************************************	
		case AUTO_REQUEST_WAIT:
		default:
			if(scheduler_getTimeElapsed(timestamp) > AUTOWAIT_TIME)
			{
				display_state = PEDESTRIAN_RED;
				timestamp = scheduler_getTime();
				mode = AUTO_REQUEST;					
			}
	}
	return display_state;	
}



static void update_leds(uint8_t display_state)
{
	switch(display_state)
	{
		case PEDESTRIAN_RED:
			GPIO_WriteHigh(CAR_G);		
			GPIO_WriteLow(CAR_Y);	
			GPIO_WriteLow(CAR_R);	
			GPIO_WriteLow(PED_G);		
			GPIO_WriteHigh(PED_R);
			break;
			
		case CAR_YELLOW:
			GPIO_WriteLow(CAR_G);		
			GPIO_WriteHigh(CAR_Y);	
			GPIO_WriteLow(CAR_R);	
			GPIO_WriteLow(PED_G);		
			GPIO_WriteHigh(PED_R);	
			break;
			
		case CAR_RED:
			GPIO_WriteLow(CAR_G);		
			GPIO_WriteLow(CAR_Y);	
			GPIO_WriteHigh(CAR_R);	
			GPIO_WriteLow(PED_G);		
			GPIO_WriteHigh(PED_R);	
			break;

		case PEDESTRIAN_GREEN:
			GPIO_WriteLow(CAR_G);		
			GPIO_WriteLow(CAR_Y);	
			GPIO_WriteHigh(CAR_R);	
			GPIO_WriteHigh(PED_G);		
			GPIO_WriteLow(PED_R);
			break;

		default : //LEDS_OFF
			GPIO_WriteLow(CAR_G);		
			GPIO_WriteLow(CAR_Y);	
			GPIO_WriteLow(CAR_R);	
			GPIO_WriteLow(PED_G);		
			GPIO_WriteLow(PED_R);	
	}
}