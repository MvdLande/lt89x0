#include "stm8s.h"
#include "stm8s_gpio.h"
#include "common.h"
#include "LT89x0.h"


enum ComState{ Idle, Transmit };
#define MAXRETRY 10

volatile uint8_t channel = DEFAULT_CHANNEL;
/*! Communication task function
 *
 *	Be aware of the following: The "real-time" scheduler used in this project
 *  runs all tasks in interrupt contect. Therefore it is assumes that a "task 
 *  function" does not contain an infinite loop. 
 *  Instead, it assumes that the task is just a function which exits when the 
 *  work is done. The task may not be put to "sleep" for a certain amount
 *  of time or loop indefinitely. Instead, the function must exit. 
 *  The "real-time" scheduler takes care of (periodically) activating the task
 *  function. 
 *  If some state must be preserved between multiple activations, then a local
 *  variable may be declared as "static" 
 *  
 */


void testMode(void);
void receiveMode(void);
void transmitMode(void);

void communication_task(void)
{
#ifdef TEST_MODE
	testMode();
#endif
	
#ifndef TRANSMIT_MODE
	receiveMode();
#else
	transmitMode();
#endif	

}

#ifndef TRANSMIT_MODE
void receiveMode(void)
{
	if (GPIO_ReadInputPin(PKT) != 0x00)
	{
		lt89x0_ReadReg(REG_STATUS, LT8920); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
		if (RegH & CRC_MASK) // CRC error
		{
			lt89x0_EnableReceiveMode(channel, LT8920);
			return;
		}
		else // read data
		{
			lt89x0_ReadReg(REG_FIFO_DATA, LT8920); //read received data (first byte = number of byte received (RegH))
			scheduler_setVar(FLAGS_HANDLE, RegL);
			lt89x0_EnableReceiveMode(channel, LT8920);
		}
	}
}
#else
void transmitMode(void)
{
	static uint8_t prev_flags = 0;
	static uint8_t flags;
	static uint8_t count = 0;
	static enum ComState com_state = Idle;
	static uint8_t TransmitedData;
	static uint8_t prev_pkt_pin = 1;
	static uint8_t pkt_pin =  1;

	flags = scheduler_getVar(FLAGS_HANDLE);
	// check if there is data to transmit

	if ((prev_flags != flags) && (flags != 0x00))
	{
		lt89x0_TransmitByte(flags, channel, LT8920);
		flags = 0x00;
		scheduler_setVar(FLAGS_HANDLE, flags);	
	}
	prev_flags = flags;

/*
	
	if (prev_flags != flags)
	{
		if (Idle == com_state)
		{
			lt89x0_TransmitByte(flags, channel, LT8920);
			//prev_pkt_pin = GPIO_ReadInputPin(PKT);
			com_state = Transmit;
			TransmitedData = flags;
			prev_flags = flags;
			count = 0;
			//return;
		}
		// wait for current transmission to end
	}
	if (Transmit == com_state)
	{
		// check for positive edge on PKT pin.
		//pkt_pin = GPIO_ReadInputPin(PKT);
		//if ( (0x00 != pkt_pin))
		lt89x0_ReadReg(REG_STATUS, LT8920); //read status register
		if (RegL & PKT_MASK) // PKT FLAG = High
		{
			// read fifo status register
			lt89x0_ReadReg(52, LT8920); 
			// if auto-ack=on then, if bits 0-5 are zero then the ack sequence was completed corretly
			if ( 0 == (RegL & 0x1F))
			{
				// transmission succesfull
				com_state = Idle;
				count = 0;
//				return;
			}
			else 
			{ 
				if (count < MAXRETRY) 
				{
					lt89x0_TransmitByte(TransmitedData, channel, LT8920);
					prev_pkt_pin = GPIO_ReadInputPin(PKT);					
					count++;
				}
				else 
				{
					// transmission unsuccessfull
					com_state = Idle;
				}
//				return;
			}
		}
	}
	prev_pkt_pin = GPIO_ReadInputPin(PKT);
	*/
}
#endif

#ifdef TEST_MODE

#define T_WAIT 10000/TICKPERIOD   /*10000ms/10ms = 20sec*/

void testMode(void)
{
	static uint16_t counter =  0;
	static uint8_t flags = 0;
	static uint8_t temp = 0;

	temp = scheduler_getVar(INIT_HANDLE);
	if (INIT_COMMUNICATIONTASK == (temp & INIT_COMMUNICATIONTASK))
	{
		counter = 0;
		scheduler_setVar(INIT_HANDLE, (temp & ~INIT_COMMUNICATIONTASK));
	}

	flags = scheduler_getVar(FLAGS_HANDLE);		
	if (T_WAIT == counter)
	{
		//simulate SW_BLINK activation
		scheduler_setVar(FLAGS_HANDLE, flags | BLINK_FLAG);		
	}
	else if (T_WAIT * 2 == counter)
	{
		//simulate SW_STOP activation
		scheduler_setVar(FLAGS_HANDLE, flags | STOP_FLAG);		
	}
	else if (T_WAIT * 3 == counter)
	{
		//simulate SW_GO activation
		scheduler_setVar(FLAGS_HANDLE, flags | GO_FLAG);		
	}	
	else if (T_WAIT * 4 == counter)	
	{
		//simulate SW_AUTO activation
		scheduler_setVar(FLAGS_HANDLE, flags | AUTO_FLAG);		
	}	
	else if (T_WAIT * 5 <= counter)	
	{
		counter = 0;		
	}		
	counter++;
}
#endif

