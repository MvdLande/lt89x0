/*	BASIC INTERRUPT VECTOR TABLE FOR STM8 devices
 *	Copyright (c) 2007 STMicroelectronics
 */
 
 // this file is used for STVD only!

typedef void @far (*interrupt_handler_t)(void);

struct interrupt_vector {
	unsigned char interrupt_instruction;
	interrupt_handler_t interrupt_handler;
};

@far @interrupt void NonHandledInterrupt (void)
{
	/* in order to detect unexpected events during development, 
	   it is recommended to set a breakpoint on the following instruction
	*/
	return;
}
extern void SysTick (void); /* SysTick timer routine */
extern void _stext();     /* startup routine */
extern void main();     /* startup routine */
extern void EXTI_handler();

struct interrupt_vector const _vectab[] = {
	{0x82, (interrupt_handler_t)_stext}, /* reset */
	{0x82, NonHandledInterrupt}, /* trap  */
	{0x82, NonHandledInterrupt}, /* irq0 TLI - External top level interrupt */
	{0x82, NonHandledInterrupt}, /* irq1 AWU - Auto wake up from halt */
	{0x82, NonHandledInterrupt}, /* irq2 CLK - Clock controller */
	{0x82, NonHandledInterrupt}, /* irq3 EXTI0 - Port A external interrupts */
	{0x82, NonHandledInterrupt}, /* irq4 EXTI1 - Port B external interrupts */
	{0x82, (interrupt_handler_t)EXTI_handler}, /* irq5 EXTI2 - Port C external interrupts */
	{0x82, (interrupt_handler_t)EXTI_handler}, /* irq6 EXTI3 - Port D external interrupts */
	{0x82, NonHandledInterrupt}, /* irq7 EXTI4 - Port E external interrupts */
	{0x82, NonHandledInterrupt}, /* irq8 reserved */
	{0x82, NonHandledInterrupt}, /* irq9 reserved */
	{0x82, NonHandledInterrupt}, /* irq10 SPI - End of transfer*/
	{0x82, NonHandledInterrupt}, /* irq11 TIM1 - TIM1 update/overflow/underflow/trigger/break */
	{0x82, NonHandledInterrupt}, /* irq12 TIM1 - TIM1 capture/compare */
	{0x82, NonHandledInterrupt}, /* irq13 TIM2 - TIM2 update /overflow */
	{0x82, NonHandledInterrupt}, /* irq14 TIM2 - TIM2 capture/compare */
	{0x82, NonHandledInterrupt}, /* irq15 reserved */
	{0x82, NonHandledInterrupt}, /* irq16 reserved */
	{0x82, NonHandledInterrupt}, /* irq17 UART1 - Tx complete */
	{0x82, NonHandledInterrupt}, /* irq18 UART1 - Receive register DATA FULL */
	{0x82, NonHandledInterrupt}, /* irq19 I2C - I2C interrupt */
	{0x82, NonHandledInterrupt}, /* irq20 reserved */
	{0x82, NonHandledInterrupt}, /* irq21 reserved */
	{0x82, NonHandledInterrupt}, /* irq22 ADC1 - ADC1 end of conversion/analog watchdog interrupt */
	{0x82, (interrupt_handler_t)SysTick}, /* irq23 TIM4 - TIM4 update/overflow */
	{0x82, NonHandledInterrupt}, /* irq24 Flash - EOP/WR_PG_DIS */
	{0x82, NonHandledInterrupt}, /* irq25 reserved */
	{0x82, NonHandledInterrupt}, /* irq26 reserved */
	{0x82, NonHandledInterrupt}, /* irq27 reserved */
	{0x82, NonHandledInterrupt}, /* irq28 reserved */
	{0x82, NonHandledInterrupt}, /* irq29 reserved */
};
