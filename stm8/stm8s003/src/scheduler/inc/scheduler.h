/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include "stm8s.h"

#define NUMTASKS 4
#define NUMVARS 2
#define TICKPERIOD 10 /*ms*/

#define scheduler_enterCriticalSection()		TIM4_ITConfig(TIM4_IT_UPDATE, DISABLE)
#define scheduler_leaveCriticalSection()		TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE)


#define DONE	0
#define INIT_COMMUNICATIONTASK	1
#define INIT_DISPLAYTASK		2
#define INIT_KEYSCANTASK		4
#define INIT_STATEMACHINETASK	8

#define FLAGS_HANDLE 0
#define INIT_HANDLE 1

typedef struct{
	volatile 	uint8_t  task_status;	/* Task status															*/
	volatile	uint16_t task_period;		/* Activation period                         		*/
	volatile 	uint16_t ticks_remaining; /* Ticks remaing untill the task is due      		*/	
  void (*task_function) (void);        	/* Function to call when the task is activated	*/
} task_t;


/* flags */
#define TASK_ENABLED 			0x01
#define	TASK_INITIALIZED	0x02
#define TASK_PENDING			0x04
#define	TASK_ACTIVATED		0x08
#define TASK_PERIODIC			0x10


enum task_priority{
	PRIO0 = 0,
	PRIO1,
	PRIO2,
	PRIO3
};

void scheduler_setVar(uint8_t var_handle, uint8_t value);
uint8_t scheduler_getVar(uint8_t var_handle);


uint16_t scheduler_getTime (void);

uint16_t scheduler_getTimeElapsed(uint16_t timestamp);

void scheduler_start (void);

void scheduler_stop (void);

void scheduler_initTasks(void);

void scheduler_createTask (void (*task_function) (void), uint8_t task_priority, uint16_t task_period, uint16_t task_phase, uint8_t task_properties);

void scheduler_enableTask (uint8_t task_priority);

void scheduler_disableTask (uint8_t task_priority);

void scheduler_set_last_event(uint16_t ticks);

uint16_t scheduler_get_last_event(void);

#endif

