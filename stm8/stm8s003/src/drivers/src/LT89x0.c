
#include "LT89x0.h"
#include "delay.h"
#include "scheduler.h"

#define WRITE		0x7F
#define READ		0x80
uint8_t Status;
uint8_t RegH;
uint8_t RegL;

//----------------------------------------------------------------------------
void lt89x0_ReadReg(uint8_t reg, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
	// claim SPI bus
	scheduler_enterCriticalSection();
	GPIO_WriteLow(SPI_NSS_port, SPI_NSS_pin);
	Status = spi_ReadWrite(READ | reg);
	RegH = spi_ReadWrite(0x00);
	RegL = spi_ReadWrite(0x00);
	GPIO_WriteHigh(SPI_NSS_port, SPI_NSS_pin);
	scheduler_leaveCriticalSection();
	// free SPI bus
}

//----------------------------------------------------------------------------


void lt89x0_WriteReg(uint8_t reg, uint8_t byteH, uint8_t byteL, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
	// claim SPI bus
	scheduler_enterCriticalSection();
	GPIO_WriteLow(SPI_NSS_port, SPI_NSS_pin);
	spi_ReadWrite(WRITE & reg);
	spi_ReadWrite(byteH);
	spi_ReadWrite(byteL);
	GPIO_WriteHigh(SPI_NSS_port, SPI_NSS_pin);
	scheduler_leaveCriticalSection();
	// free SPI bus
}


void lt89x0_TransmitByte(uint8_t Byte, uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
  lt89x0_WriteReg(REG_TXRX_CTRL, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin); /* disable send and receive mode */
  //DelayMs (3);
  lt89x0_WriteReg(REG_FIFO_CTRL, 0x80, 0x00, SPI_NSS_port, SPI_NSS_pin); /* clear transmit buffer (FIFO Poin
  lt89x0_WriteReg(REG_FIFO_CTRL, 0x80, 0x00, SPI_NSS_port, SPI_NSS_pin);  * ter Clear) */
	/* construct packet, first byte is packet size */	
  lt89x0_WriteReg(REG_FIFO_DATA, 0x01, Byte, SPI_NSS_port, SPI_NSS_pin); /* Packet size = One byte */
	//lt89x0_WriteReg(7, 0x01, 0x30, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_TXRX_CTRL, 0x01, (channel & 0xef), SPI_NSS_port, SPI_NSS_pin); /* start transmission on channel, frequency = 2402 + channel MHz */
}

void lt89x0_EnableReceiveMode(uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
  lt89x0_WriteReg(REG_TXRX_CTRL, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin); /* disable send and receive mode */
	//DelayMs (3);
  lt89x0_WriteReg(REG_FIFO_CTRL, 0x00, 0x80, SPI_NSS_port, SPI_NSS_pin); /* clear receive buffer */
	//lt89x0_WriteReg(7, 0x00, 0xB0, SPI_NSS_port, SPI_NSS_pin); //enable receive mode, select channel 30
  lt89x0_WriteReg(REG_TXRX_CTRL, 0x00, 0x80 | (channel & 0xef), SPI_NSS_port, SPI_NSS_pin); /* enable receive mode, set channel No., frequency = 2402 + channel MHz */
}

void lt89x0_WriteConfig(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
  lt89x0_WriteReg(REG_MAGIC0, 0x6f, 0xe0, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_MAGIC1, 0x56, 0x81, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_MAGIC2, 0x66, 0x17, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_MAGIC4, 0x9c, 0xc9, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_MAGIC5, 0x66, 0x37, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_TXRX_CTRL, 0x00, 0x30, SPI_NSS_port, SPI_NSS_pin); // set channel# = 48
  lt89x0_WriteReg(REG_MAGIC8, 0x6c, 0x90, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_PA_CFG, 0x48, 0x00, SPI_NSS_port, SPI_NSS_pin);		// MAX transmit power
  lt89x0_WriteReg(REG_OSC_CFG, 0x7f, 0xfd, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_RSSI_CFG, 0x00, 0x08, SPI_NSS_port, SPI_NSS_pin); //RSSI ENABLE
  lt89x0_WriteReg(REG_MAGIC12, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_MAGIC13, 0x48, 0xbd, SPI_NSS_port, SPI_NSS_pin);

  lt89x0_WriteReg(REG_MAGIC22, 0x00, 0xff, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_VCO_CFG, 0x80, 0x05, SPI_NSS_port, SPI_NSS_pin); // Calibrate VCO before each RX/TX
  lt89x0_WriteReg(REG_MAGIC24, 0x00, 0x67, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_MAGIC25, 0x16, 0x59, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_MAGIC26, 0x19, 0xe0, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_TRIM, 0x13, 0x00, SPI_NSS_port, SPI_NSS_pin); // No cristal trim
  lt89x0_WriteReg(REG_MAGIC28, 0x18, 0x00, SPI_NSS_port, SPI_NSS_pin);

  //lt89x0_WriteReg(REG_PKT_CFG1, 0x50, 0x00, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_PKT_CFG1, 0x48, 0x08, SPI_NSS_port, SPI_NSS_pin); // Packet data type = NRZ BRCLK = 12/4=3MHZ
  lt89x0_WriteReg(REG_VCO_PA_TIMING1, 0x3f, 0xc7, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_VCO_PA_TIMING2, 0x20, 0x00, SPI_NSS_port, SPI_NSS_pin);
  //lt89x0_WriteReg(REG_PWR_PKT_CFG, 0x03, 0x00, SPI_NSS_port, SPI_NSS_pin); // max 3 re-transmissions when auto-ack=on
  lt89x0_WriteReg(REG_PWR_PKT_CFG, 0x0F, 0x00, SPI_NSS_port, SPI_NSS_pin); // max 15 re-transmissions, auto-ack=on
  lt89x0_WriteReg(REG_SYNC_WORD0, 0x03, 0x80, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_SYNC_WORD1, 0x03, 0x80, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_SYNC_WORD2, 0x5a, 0x5a, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_SYNC_WORD3, 0x03, 0x80, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_FIFO_SYNCW_CFG, 0x21, 0x02, SPI_NSS_port, SPI_NSS_pin); // config FIFO FLAG, allow 2-1=1 syncword bit errors
  //lt89x0_WriteReg(REG_FIFO_SYNCW_CFG, 0x44, 0x01, SPI_NSS_port, SPI_NSS_pin); // allow 1-1=0 syncword bit errors  
  //lt89x0_WriteReg(REG_PKT_CFG2, 0xB0, 0x00, SPI_NSS_port, SPI_NSS_pin);  //crc on scramble off ,1st byte packet length ,auto ack off, PKT active high
	lt89x0_WriteReg(REG_PKT_CFG2, 0xB8, 0x00, SPI_NSS_port, SPI_NSS_pin); //crc on, scramble off, 1st byte = packet length, auto ack on, PKT active high
  lt89x0_WriteReg(REG_RSSI_ACK_CFG, 0xfd, 0xb0, SPI_NSS_port, SPI_NSS_pin);  //
  lt89x0_WriteReg(REG_RSSI_CTRL, 0x00, 0x0f, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(REG_DATARATE_CFG, 0x01, 0x00, SPI_NSS_port, SPI_NSS_pin); // set datarate = 1Mbps 
  lt89x0_WriteReg(REG_OPTION_CFG, 0x00, 0x80, SPI_NSS_port, SPI_NSS_pin); // Best value is 0080H when data rate is 1Mbps
	
  //lt89x0_WriteReg(REG_DATARATE_CFG, 0x04, 0x00, SPI_NSS_port, SPI_NSS_pin); // set datarate = 256Kbps 
  //lt89x0_WriteReg(REG_OPTION_CFG, 0x05, 0x52, SPI_NSS_port, SPI_NSS_pin); // Best value is 0552H when data rate is not 1Mbps
	
  //lt89x0_WriteReg(REG_FIFO_DATA, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin);
	//DelayMs(200);

  //lt89x0_WriteReg(REG_TXRX_CTRL, 0x01, 0x00, SPI_NSS_port, SPI_NSS_pin); /* start transmission on channel 0, frequency = 2402 + 0 = 2.402GHz */
	//DelayMs(2);
  //lt89x0_WriteReg(REG_TXRX_CTRL, 0x00, 0x30, SPI_NSS_port, SPI_NSS_pin); /* disable send and receive mode, set channel = 0x30 (48) */
	
}

//----------------------------------------------------------------------------
void lt89x0_Init(void)
{
	GPIO_Init(CE, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(PKT, GPIO_MODE_IN_PU_NO_IT);
	GPIO_Init(NSS, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_WriteHigh(NSS);
	GPIO_WriteLow(CE);	
	DelayMs(1);
	GPIO_WriteHigh(CE);	
	DelayMs(5);
	lt89x0_WriteConfig(NSS);
	
#ifdef SECOND_LT8920
	GPIO_Init(CE2, GPIO_MODE_OUT_PP_HIGH_SLOW);	
	GPIO_Init(PKT2, GPIO_MODE_IN_PU_NO_IT);
	GPIO_Init(NSS2, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_WriteHigh(NSS2);	
	GPIO_WriteLow(CE2);	
	DelayMs(1);
	GPIO_WriteHigh(CE2);		
	DelayMs(5);
	lt89x0_WriteConfig(NSS2);
#endif

}

void lt89x0_DeInit(void)
{
	GPIO_WriteHigh(NSS);
	GPIO_WriteLow(CE);	
	
#ifdef SECOND_LT8920
	GPIO_WriteHigh(NSS2);	
	GPIO_WriteLow(CE2);	
#endif

}
