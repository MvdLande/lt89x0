/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LT89X0_H
#define __LT89X0_H

#include "stm8s.h"
#include "stm8s_gpio.h"
#include "spi.h"

extern uint8_t Status, RegH, RegL;


#define DEFAULT_CHANNEL 48
#define CRC_MASK 0x80
#define FIFO_MASK 0x10
#define PKT_MASK 0x20
#define SYNCW_MASK 0x40


//#define SECOND_LT8920

//LT8920 Chip Enable (nRESET) pin definitions
#define CE	 	GPIOA,GPIO_PIN_1 	/*PA1*/ 
//LT8920 PKT interrupt pin definitions
#define PKT 	GPIOA,GPIO_PIN_2 	/*PA2*/
//LT8920 NSS pin definition
#define NSS	 	GPIOA,GPIO_PIN_3 	/*PA3*/ 

#ifdef SECOND_LT8920
//LT8920 Chip Enable (nRESET) pin definitions
#define CE2	 	GPIOB,GPIO_PIN_4 	/*SCL (pullup resistor required)*/
#define PKT2 	GPIOB,GPIO_PIN_5 	/*SDA (pullup resistor required)*/ 
#define NSS2 	GPIOD,GPIO_PIN_5 	/*PD5*/ 
#endif

#define REG_MAGIC0 0
#define REG_MAGIC1 1
#define REG_MAGIC2 2
#define REG_RF_STATUS 3
#define REG_MAGIC4 4
#define REG_MAGIC5 5
#define REG_RSSI_STATUS 6
#define REG_TXRX_CTRL 7
#define REG_MAGIC8 8
#define REG_PA_CFG 9
#define REG_OSC_CFG 10
#define REG_RSSI_CFG 11
#define REG_MAGIC12 12
#define REG_MAGIC13 13
#define REG_MAGIC22 22
#define REG_VCO_CFG 23
#define REG_MAGIC24 24
#define REG_MAGIC25 25
#define REG_MAGIC26 26
#define REG_TRIM 27
#define REG_MAGIC28 28
#define REG_HW_ID 29
#define REG_JEDEC_L 30
#define REG_JEDEC_H 31
#define REG_PKT_CFG1 32
#define REG_VCO_PA_TIMING1 33
#define REG_VCO_PA_TIMING2 34
#define REG_PWR_PKT_CFG 35
#define REG_SYNC_WORD0 36
#define REG_SYNC_WORD1 37
#define REG_SYNC_WORD2 38
#define REG_SYNC_WORD3 39
#define REG_FIFO_SYNCW_CFG 40
#define REG_PKT_CFG2 41
#define REG_RSSI_ACK_CFG 42
#define REG_RSSI_CTRL 43
#define REG_DATARATE_CFG 44
#define REG_OPTION_CFG 45
#define REG_STATUS 48
#define REG_FIFO_DATA 50
#define REG_FIFO_CTRL 52




//----------------------------------------------------------------------------
void lt89x0_ReadReg(uint8_t reg, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_WriteReg(uint8_t reg, uint8_t byteH, uint8_t byteL, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_TransmitByte(uint8_t Byte, uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_EnableReceiveMode(uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_WriteConfig(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_Init(void);
void lt89x0_DeInit(void);

#endif

