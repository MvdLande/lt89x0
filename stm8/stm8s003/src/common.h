/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMMON_H
#define __COMMON_H

#include "stm8s.h"
#include "scheduler.h"
#include "IO.h"
#include "shared_memory.h"

#define MAX7219 NSS2
#define LT8920	NSS

#define WALK_FLAG 	0x01
#define GO_FLAG 	0x02
#define STOP_FLAG 	0x04
#define BLINK_FLAG 	0x08
#define AUTO_FLAG 	0x10

// uncomment the following line to enable the sender functions (remote controller)
//#define TRANSMIT_MODE	/*enable the sender part of the program */

#define DISABLE_SCHEDULER

//#define TEST_MODE

#endif