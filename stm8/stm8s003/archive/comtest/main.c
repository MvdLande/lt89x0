#include "stm8s.h"
#include "stm8s_gpio.h"
#include "stm8s_spi.h"
#include "stm8s_i2c.h"

// LT8920 Slave Select (NSS) port,pin definitions
#define SPI_NSS_LT8920_onboard 	GPIOA,GPIO_PIN_3 	/*PA3*/ 
#define SPI_NSS_LT8920_extra 	GPIOD,GPIO_PIN_5 		/*PD5*/
// LT8920 CE pin
#define SPI_CE_LT8920_onboard 	GPIOA,GPIO_PIN_1 	/*PA1*/
#define SPI_CE_LT8920_extra 	GPIOB,GPIO_PIN_4 		/*SCL*/
// LT8920 PKT pin definitions
#define PKT_LT8920_onboard 	GPIOA,GPIO_PIN_2 			/*PA2*/
#define PKT_LT8920_extra 		GPIOB,GPIO_PIN_5			/*SDA*/

void delay (int ms){ //Function Definition
  int i =0 ;
  int j=0;
  for (i=0; i<=ms; i++)
  {
    for (j=0; j<120; j++) // Nop = Fosc/4
      _asm("nop"); //Perform no operation //assembly code              
  }
}


void SPI_Setup(void){
     SPI_DeInit();
     SPI_Init(SPI_FIRSTBIT_MSB,
              SPI_BAUDRATEPRESCALER_2,
              SPI_MODE_MASTER,
              SPI_CLOCKPOLARITY_LOW,
              SPI_CLOCKPHASE_1EDGE,
              SPI_DATADIRECTION_2LINES_FULLDUPLEX,
              SPI_NSS_SOFT,
              0x01);
     SPI_Cmd(ENABLE);
		 GPIO_Init(SPI_NSS_LT8920_onboard, GPIO_MODE_OUT_PP_LOW_SLOW);
		 GPIO_WriteHigh(SPI_NSS_LT8920_onboard);  

		 GPIO_Init(SPI_NSS_LT8920_extra, GPIO_MODE_OUT_PP_LOW_SLOW);
		 GPIO_WriteHigh(SPI_NSS_LT8920_extra);
}

//LT89x0 definitions

struct LT89x0_reg {
	uint8_t Address;
	uint16_t Data;
};	

const struct LT89x0_reg TestPacket[] = {
	{7, 0x0030},	/* disable send and receive mode */
	{52, 0x8000},	/* clear transmit buffer */
  /* construct packet, first byte is packet size */	
	{50, 0x01e0},	/* Packet size = One byte, value 0xe0 */
	/* start transmission */
	{7, 0x0130}		/* start transmission on channel 48, frequency = 2402 + 48 = 2.45GHz */
};

const struct LT89x0_reg EnableReceiveMode[] = {
	{7, 0x0030},	/* disable send and receive mode */
	{52, 0x0080},	/* clear reeive buffer */
	{7, 0x00B0}		/* start listening on channel 48, frequency = 2402 + 48 = 2.45GHz */
};

const struct LT89x0_reg EnableTransmitMode[] = {
	{7, 0x0030},	/* disable send and receive mode */
	{52, 0x8000}	/* clear reeive buffer */
};

struct LT89x0_reg Register[] = {
	{29, 0x0000} // Silicon version
};

const struct LT89x0_reg LT89x0_config[] = {
	{0, 0x6fe0},
  {1, 0x5681},  
  {2, 0x6617},  
  {4, 0x9cc9},  
  {5, 0x6637},  
  {7, 0x0030},  /* disable send and receive mode, set channel = 0x30 (48) */
  {8, 0x6c90},
  {9, 0x4800},	// 5.5dBm
  {10, 0x7ffd},
  {11, 0x0008},
  {12, 0x0000},
  {13, 0x48bd},
  {22, 0x00ff},
  {23, 0x8005},
  {24, 0x0067},
  {25, 0x1659},
  {26, 0x19e0},
  {27, 0x1300}, //------------ 1200
  {28, 0x1800},
  {32, 0x4808}, //5000 //5004
  {33, 0x3fc7}, 
  {34, 0x2000},
  {35, 0x0300}, //0F00  0320
  {36, 0x1234}, // syncword 1
  {37, 0x5678}, // syncword 2
  {38, 0x9abc}, // syncword 3
  {39, 0xdef0}, // syncword 4
  {40, 0x2102}, //4401
  {41, 0xB800}, //crc on, scramble off, 1st byte = packet length, auto ack on
  {42, 0xfdb0}, //176us
  {43, 0x000f},
  {44, 0x1000}, // 65.5kbps
  {45, 0x0552}, //
	{52, 0x8080},	/* clear transmit buffer and receive buffer */	
  {50, 0x0000}  // dummy packet of 0 bytes
}; //35 registers


uint8_t LT89x0_RegWrite(struct LT89x0_reg *Register, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
    uint8_t status;
		while(SPI_GetFlagStatus(SPI_FLAG_BSY));
    GPIO_WriteLow(SPI_NSS_port, SPI_NSS_pin); 
		
    SPI_SendData(Register->Address & 0xEF); // write mode
    while(!SPI_GetFlagStatus(SPI_FLAG_TXE)); 
		status = SPI_ReceiveData();
		if (Register->Address == 50)
			delay(1); // delay for fifo address selection
		
    SPI_SendData((uint8_t)((Register->Data>>8) & 0xFF)); //upper byte
    while(!SPI_GetFlagStatus(SPI_FLAG_TXE)); 
		
    SPI_SendData((uint8_t) (Register->Data & 0xFF)); //lower byte
    while(!SPI_GetFlagStatus(SPI_FLAG_TXE));	
		
    GPIO_WriteHigh(SPI_NSS_port, SPI_NSS_pin);
		return status;
}

void LT89x0_RegBulkWrite(struct LT89x0_reg *Register, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin ){
	uint8_t i;
	for(i=0; i<sizeof(Register); i++){
		LT89x0_RegWrite(&Register[i], SPI_NSS_port, SPI_NSS_pin); /* send test data */
	}
}


uint8_t LT89x0_RegRead(struct LT89x0_reg *Register, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
    uint8_t status;
		uint16_t data = 0;
		while(SPI_GetFlagStatus(SPI_FLAG_BSY));
    GPIO_WriteLow(SPI_NSS_port, SPI_NSS_pin); 
		
    SPI_SendData(Register->Address | 0x80); // read mode
    while(!SPI_GetFlagStatus(SPI_FLAG_TXE));
		status = SPI_ReceiveData();
		if (Register->Address == 50)
			delay(1); // delay for fifo address selection
			
    SPI_SendData((uint8_t)((Register->Data>>8) & 0xFF)); //upper byte
    while(!SPI_GetFlagStatus(SPI_FLAG_TXE));
		data = SPI_ReceiveData()<<8;
		
    SPI_SendData((uint8_t) (Register->Data & 0xFF)); //lower byte
    while(!SPI_GetFlagStatus(SPI_FLAG_TXE));	
		data =  data + SPI_ReceiveData();	
		
    GPIO_WriteHigh(SPI_NSS_port, SPI_NSS_pin);
		Register->Data = data;
		return status;
}

void LT89x0_Init(void){
	GPIO_Init(SPI_CE_LT8920_onboard, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(SPI_CE_LT8920_extra, GPIO_MODE_OUT_PP_HIGH_SLOW);
	
	GPIO_Init(SPI_NSS_LT8920_onboard, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(SPI_NSS_LT8920_extra, GPIO_MODE_OUT_PP_HIGH_SLOW);
	
	GPIO_Init(PKT_LT8920_onboard, GPIO_MODE_IN_PU_NO_IT);
	GPIO_Init(PKT_LT8920_extra, GPIO_MODE_IN_PU_NO_IT);
	
	GPIO_WriteHigh(SPI_NSS_LT8920_onboard);	
	GPIO_WriteHigh(SPI_NSS_LT8920_extra);	
	
	GPIO_WriteLow(SPI_CE_LT8920_onboard);
	GPIO_WriteLow(SPI_CE_LT8920_extra);
	delay(50);
	GPIO_WriteHigh(SPI_CE_LT8920_onboard);	
	GPIO_WriteHigh(SPI_CE_LT8920_extra);		
	delay(50);
	LT89x0_RegBulkWrite(&LT89x0_config[0], SPI_NSS_LT8920_onboard);
	LT89x0_RegBulkWrite(&LT89x0_config[0], SPI_NSS_LT8920_extra);
	delay(10);
}



void main(){
	uint8_t status;
	I2C_DeInit();
	GPIO_DeInit(GPIOA); // prepare Port A for working
	GPIO_DeInit(GPIOB); // prepare Port B for working
	GPIO_DeInit(GPIOC); // prepare Port C for working
	GPIO_DeInit(GPIOD); // prepare Port C for working	
	SPI_Setup();
	LT89x0_Init();
	status = LT89x0_RegRead(&Register[0], SPI_NSS_LT8920_extra); // read and write syncword 1 data
	
	// set extra LT8920 in receive mode
	LT89x0_RegBulkWrite(&EnableReceiveMode[0], SPI_NSS_LT8920_onboard);
	delay(20);
	while (GPIO_ReadInputPin(PKT_LT8920_extra)); //wait if PKT is high (it should be low)  
	// transmit a test packet of one byte 
	LT89x0_RegBulkWrite(&TestPacket[0], SPI_NSS_LT8920_onboard);
	
	while (!GPIO_ReadInputPin(PKT_LT8920_extra)); //wait for PKT to go high  
	
	
	// BitStatus GPIO_ReadInputPin(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin)

	
	//Declare PA2  as input pull up pin
	//GPIO_Init (GPIOA, GPIO_PIN_2, GPIO_MODE_IN_PU_IT);

	//Declare PB5 as push pull Output pin
  //GPIO_Init (GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

  //GPIO_Init (GPIOC, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);

	//GPIO_WriteHigh(GPIOC, GPIO_PIN_3); //LED ON
	
  while(1){
		//GPIO_WriteReverse(GPIOB,GPIO_PIN_5);
		//GPIO_WriteLow(GPIOC, GPIO_PIN_3); //LED OFF
		delay (300);
		//GPIO_WriteHigh(GPIOC, GPIO_PIN_3); //LED ON
		delay (300);
	}

}


#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif