/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMMUNICATIONTASK_H
#define __COMMUNICATIONTASK_H

extern volatile uint8_t channel;

void communication_task(void);

#endif