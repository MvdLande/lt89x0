/** @file 1keypress_filter_logic.h
*
* @brief Debounce/ Glitch filter logic for filering an input pin.
*
* @par
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __keypress_FILTER_LOGIC_H
#define __keypress_FILTER_LOGIC_H
#include "stm8s.h"

#define MAX_KEY_TICKS 0xFFFF


enum event {
	KEY_RELEASED = 0,
	KEY_ACTIVATED,
	NO_EVENT
};


// in order to conserve data memory, the variable's width has been compressed 
// to fit three 8-bit integers into one byte 
typedef struct{
	uint8_t  output:1;	// filtered input level
	uint8_t  level_changed_tick_counter:4;				// level change duration
	uint8_t  event:2; 					// level change event
	uint8_t	 invert_input:1;
	uint16_t level_stable_tick_counter;
} keypress_filter_state_t;

void keypress_filter_init(keypress_filter_state_t *filter, bool invert_input);
void keypress_filter (uint8_t input, keypress_filter_state_t *filter);

#endif