#include "stm8s.h"

#define EnterCriticalSection()		TIM4_ITConfig(TIM4_IT_UPDATE, DISABLE)
#define LeaveCriticalSection()		TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE)


#define NUMTASKS 4

typedef struct Task *Taskp;   	 /* task pointer                              */

typedef struct Task {
	volatile 	uint8_t  Settings;	/* Task settings																*/
	volatile	uint16_t Period;		/* Activation period                         		*/
	volatile 	uint16_t Remaining; /* Ticks remaing untill the task is due      		*/	
  void (*Taskf) (void);        	/* Function to call when the task is activated	*/
} Task;

/* flags */
#define TaskEnabled 		0x01
#define	TaskInitialized	0x02
#define TaskPending			0x04
#define	TaskActivated		0x08
#define TaskPeriodic		0x10

extern Task Tasklist[NUMTASKS];

void InitTasks(void);

void CreateTask (void (*Taskf) (void), uint8_t Priority, uint16_t Period, uint16_t Phase, uint8_t Flags);

void EnableTask (uint8_t Priority);

void DisableTask (uint8_t Priority);

