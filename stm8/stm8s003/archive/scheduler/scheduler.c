#include "scheduler.h"
#include "spi.h"

Task Tasklist[NUMTASKS];

void InitTasks(void){
	uint8_t i;
	for (i=0;i<NUMTASKS;i++){
		Taskp t = &Tasklist[i];	
		t->Settings = 0x00;
	}
}

void CreateTask (void (*Taskf) (void), uint8_t Priority, uint16_t Period, uint16_t Phase, uint8_t Flags){
	Taskp t = &Tasklist[Priority];
	t->Period = Period;
	t->Remaining = Phase;
	t->Taskf = Taskf;
	t->Settings = Flags | TaskInitialized;
}

void EnableTask (uint8_t Priority){
	Taskp t = &Tasklist[Priority];
	t->Settings |= TaskEnabled;
}

void DisableTask (uint8_t Priority){
	Taskp t = &Tasklist[Priority];
	t->Settings &= ~TaskEnabled;
}

void CheckPendingTasks (void){
	uint8_t i; 
	for (i=0;i<NUMTASKS;i++) {
    Taskp t = &Tasklist[i];
		if (t->Settings & TaskEnabled){ // countdown
      if (t->Remaining-- == 0) {
				if (t->Settings & TaskPeriodic)
					t->Remaining = t->Period-1; 	// load next activation period
				else	
					t->Settings &= ~TaskEnabled;  //disable task
				t->Settings |= TaskPending;	// set Pending flag		
			}
		}
	}		
}

void RunPendingTasks (void){
	// activated tasks can be interrupted by pending higher priority tasks 
  uint8_t i = NUMTASKS-1; 
  for (i=0;i<NUMTASKS;i++) {
    Taskp t = &Tasklist[i];
    if (t->Settings & TaskPending){	
			if (t->Settings & TaskActivated) // task has already been activated
				break;												 // do not activate it again
			t->Settings &= ~TaskPending;	// clear Pending flag				
			t->Settings |= TaskActivated; // set Activated flag
			t->Taskf();										// run the task function
			t->Settings &= ~TaskActivated;// clear Activated flag
		}
  }
}

@far @interrupt void SysTick (void)
{
  static uint16_t count;
	CheckPendingTasks();
	TIM4_ClearITPendingBit(TIM4_IT_UPDATE);	
	RunPendingTasks();
	
/*
	if(count == 0){
		GPIO_WriteLow(NSS);
	}		
	if(count == 50){
		GPIO_WriteHigh(NSS);		
	}
	count++;
	if(count > 100){
		count = 0;
	}
*/
}
