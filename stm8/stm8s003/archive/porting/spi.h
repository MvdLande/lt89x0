/*
软件版权所有：深圳市安信可科技有限公司		2013年11月27日发行

中国最大最便宜的 的2.4G模块 生产原厂

技术支持热线：4008 555 368  0755-61195776  15323435161  赵工

批量报价：10K以下2.6RMB 50K以上2.5RMB

特性：128个频道可调  空旷距离120-150米  低功耗   

官网：http://www.ai-thinker.com

厂址：深圳 宝安 西乡 固戍 恒南路 新园工业区六号四楼 安信可科技
*/
#ifndef _SPI_H
#define _SPI_H

//#include "stc15l204ea.h"
#include "stm8s.h"
#include "stm8s_gpio.h"

extern uint8_t Status, RegH, RegL;

#define WRITE		0x7F
#define READ		0x80

//sbit	TESTLED	= P3^7;
//sbit	TXLED	= P1^7;
//sbit	RXLED	= P1^6;

//sbit	RESET_N	= P1^5;  			//output
//sbit	SS 		= P1^4;       		//output
//sbit	MOSI 	= P1^3;     		//output
//sbit	SCLK	= P1^2;     		//output
//sbit	PKT 	= P1^1;  			//input
//sbit	MISO 	= P1^0;       		//input

#define CE	 	GPIOA,GPIO_PIN_1 	/*PA1*/ 
#define CE2	 	GPIOB,GPIO_PIN_4 	/*SCL (pullup resistor required)*/ 
#define NSS	 	GPIOA,GPIO_PIN_3 	/*PA3*/ 
#define NSS2 	GPIOD,GPIO_PIN_5 	/*PD5*/ 
#define MISO 	GPIOC,GPIO_PIN_7 	/*PC7*/
#define MOSI 	GPIOC,GPIO_PIN_6 	/*PC6*/
#define SCK 	GPIOC,GPIO_PIN_5 	/*PC5*/
#define PKT 	GPIOA,GPIO_PIN_2 	/*PA2*/
#define PKT2 	GPIOB,GPIO_PIN_5 	/*SDA (pullup resistor required)*/ 
void InitLT8900(void);
void spiWriteReg(uint8_t reg, uint8_t byteH, uint8_t byteL, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void spiReadreg(uint8_t reg, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
uint8_t spiReadWrite(uint8_t Byte);

void spiWriteKey(uint8_t Key, uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void spiEnableReceiveMode(uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );

#endif
