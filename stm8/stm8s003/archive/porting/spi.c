
#include "spi.h"
#include "delay.h"
#include "pin_definitions.h"


uint8_t Status, RegH, RegL;

//-----------------------------------------------------------------------------
uint8_t spiReadWrite(uint8_t Byte)
{
	uint8_t i;

   for (i = 0; i < 8; i++)
   {
      if((Byte & 0x80) != 0x00)
				GPIO_WriteHigh(MOSI);	
			else
				GPIO_WriteLow(MOSI);	
			GPIO_WriteHigh(SCK);
			Delay10us();
      Byte <<= 1;
      if( GPIO_ReadInputPin(MISO) != 0x00)			
				Byte |= 0x01;
			GPIO_WriteLow(SCK);
			Delay10us();
    }
	return (Byte);
}

//void LT89x0_RegBulkWrite(struct LT89x0_reg *Register, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin ){
//	uint8_t i;
//	for(i=0; i<sizeof(Register); i++){
//		LT89x0_RegWrite(&Register[i], SPI_NSS_port, SPI_NSS_pin); /* send test data */
//	}
//}

//----------------------------------------------------------------------------
void spiReadreg(uint8_t reg, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin ){
	GPIO_WriteLow(SPI_NSS_port, SPI_NSS_pin);
	Status = spiReadWrite(READ | reg);
  RegH = spiReadWrite(0x00);
  RegL = spiReadWrite(0x00);
	GPIO_WriteHigh(SPI_NSS_port, SPI_NSS_pin);
}

//----------------------------------------------------------------------------


void spiWriteReg(uint8_t reg, uint8_t byteH, uint8_t byteL, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
	GPIO_WriteLow(SPI_NSS_port, SPI_NSS_pin);
	spiReadWrite(WRITE & reg);
  spiReadWrite(byteH);
  spiReadWrite(byteL);
	GPIO_WriteHigh(SPI_NSS_port, SPI_NSS_pin);
}

void spiWriteKey(uint8_t Key, uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
  spiWriteReg(7, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin); /* disable send and receive mode */
	DelayMs (3);
  spiWriteReg(52, 0x80, 0x00, SPI_NSS_port, SPI_NSS_pin); /* clear transmit buffer */
	/* construct packet, first byte is packet size */	
  spiWriteReg(50, 0x01, Key, SPI_NSS_port, SPI_NSS_pin); /* Packet size = One byte */
	//spiWriteReg(7, 0x01, 0x30, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(7, 0x01, channel & 0xef, SPI_NSS_port, SPI_NSS_pin); /* start transmission on channel, frequency = 2402 + channel MHz */
}

void spiEnableReceiveMode(uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
  spiWriteReg(7, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin); /* disable send and receive mode */
	DelayMs (3);
  spiWriteReg(52, 0x00, 0x80, SPI_NSS_port, SPI_NSS_pin); /* clear receive buffer */
	//spiWriteReg(7, 0x00, 0xB0, SPI_NSS_port, SPI_NSS_pin); //enable receive mode, select channel 30
  spiWriteReg(7, 0x00, 0x80 | (channel & 0xef), SPI_NSS_port, SPI_NSS_pin); /* enable receive mode, set channel No., frequency = 2402 + channel MHz */
}

void spiWriteConfig(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin ){

  spiWriteReg(0, 0x6f, 0xe0, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(1, 0x56, 0x81, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(2, 0x66, 0x17, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(4, 0x9c, 0xc9, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(5, 0x66, 0x37, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(7, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(8, 0x6c, 0x90, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(9, 0x48, 0x00, SPI_NSS_port, SPI_NSS_pin);				// 5.5dBm
  spiWriteReg(10, 0x7f, 0xfd, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(11, 0x00, 0x08, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(12, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(13, 0x48, 0xbd, SPI_NSS_port, SPI_NSS_pin);

  spiWriteReg(22, 0x00, 0xff, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(23, 0x80, 0x05, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(24, 0x00, 0x67, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(25, 0x16, 0x59, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(26, 0x19, 0xe0, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(27, 0x13, 0x00, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(28, 0x18, 0x00, SPI_NSS_port, SPI_NSS_pin);

  spiWriteReg(32, 0x50, 0x00, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(33, 0x3f, 0xc7, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(34, 0x20, 0x00, SPI_NSS_port, SPI_NSS_pin);
  //spiWriteReg(35, 0x03, 0x00, SPI_NSS_port, SPI_NSS_pin); // max 3 re-transmissions when auto-ack=on
  spiWriteReg(35, 0x0F, 0x00, SPI_NSS_port, SPI_NSS_pin); // max 15 re-transmissions when auto-ack=on
  spiWriteReg(36, 0x03, 0x80, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(37, 0x03, 0x80, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(38, 0x5a, 0x5a, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(39, 0x03, 0x80, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(40, 0x44, 0x07, SPI_NSS_port, SPI_NSS_pin); // allow 7-1=6 syncword bit errors
  //spiWriteReg(41, 0xB0, 0x00, SPI_NSS_port, SPI_NSS_pin);  //crc on scramble off ,1st byte packet length ,auto ack off
	spiWriteReg(41, 0xB8, 0x00, SPI_NSS_port, SPI_NSS_pin); //crc on, scramble off, 1st byte = packet length, auto ack on
  spiWriteReg(42, 0xfb, 0xb0, SPI_NSS_port, SPI_NSS_pin);  //
  spiWriteReg(43, 0x00, 0x0f, SPI_NSS_port, SPI_NSS_pin);
  spiWriteReg(44, 0x01, 0x00, SPI_NSS_port, SPI_NSS_pin); // set datarate = 1Mbps 
  spiWriteReg(45, 0x00, 0x80, SPI_NSS_port, SPI_NSS_pin); // Best value is 0080H when data rate is 1Mbps
  //spiWriteReg(44, 0x04, 0x00, SPI_NSS_port, SPI_NSS_pin); // set datarate = 256Kbps 
  //spiWriteReg(45, 0x05, 0x52, SPI_NSS_port, SPI_NSS_pin); // Best value is 0552H when data rate is not 1Mbps
	
  spiWriteReg(50, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin);
	DelayMs(200);

  spiWriteReg(7, 0x01, 0x00, SPI_NSS_port, SPI_NSS_pin); /* start transmission on channel 0, frequency = 2402 + 0 = 2.402GHz */
	DelayMs(2);
  spiWriteReg(7, 0x00, 0x30, SPI_NSS_port, SPI_NSS_pin); /* disable send and receive mode, set channel = 0x30 (48) */
	
}

//----------------------------------------------------------------------------
void InitLT8900(void)
{
	GPIO_Init(CE, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(CE2, GPIO_MODE_OUT_PP_HIGH_SLOW);	
	GPIO_Init(NSS, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(NSS2, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(MISO,  GPIO_MODE_IN_PU_NO_IT);
	GPIO_Init(MOSI, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(SCK, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(PKT, GPIO_MODE_IN_PU_NO_IT);
	GPIO_Init(PKT2, GPIO_MODE_IN_PU_NO_IT);
	GPIO_WriteHigh(NSS);	
	GPIO_WriteHigh(NSS2);	
	GPIO_WriteLow(SCK);	
	
	GPIO_WriteLow(CE);	
	GPIO_WriteLow(CE2);	
  DelayMs(100);
	GPIO_WriteHigh(CE);	
	GPIO_WriteHigh(CE2);		
  DelayMs(200);

	spiWriteConfig(NSS);
	spiWriteConfig(NSS2);
}
