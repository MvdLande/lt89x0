#include "stm8s.h"
#include "stm8s_gpio.h"
#include "stm8s_spi.h"
#include "stm8s_i2c.h"
#include "spi.h"
#include "pin_definitions.h"



void DelayMs (uint16_t ms){ //Function Definition
  uint16_t i =0 ;
  uint8_t j=0;
  for (i=0; i<=ms; i++)
  {
    for (j=0; j<120; j++) // Nop = Fosc/4
      _asm("nop"); //Perform no operation //assembly code              
  }
}

Delay10us(void)
{
	uint8_t j=0;
	for (j=0; j<12; j++) // Nop = Fosc/4
		_asm("nop"); //Perform no operation //assembly code              
}

void DeInit_GPIO(void)
{
	GPIO_DeInit(GPIOA); // prepare Port A for working
	GPIO_DeInit(GPIOB); // prepare Port B for working
	GPIO_DeInit(GPIOC); // prepare Port C for working
	GPIO_DeInit(GPIOD); // prepare Port D for working	
}


void main(){
	uint8_t status, channel;
	channel = 100;
	DeInit_GPIO();
	InitLT8900();
	spiReadreg(7, NSS);
	spiReadreg(7, NSS2);
	spiEnableReceiveMode(channel,NSS2);
	spiWriteKey(0xe0, channel, NSS);
	spiReadreg(48, NSS2); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
	spiReadreg(50, NSS2); //read received data


	//while (GPIO_ReadInputPin(PKT_LT8920_extra)); //wait if PKT is high (it should be low)  
	// transmit a test packet of one byte 
		
	
	//BitStatus GPIO_ReadInputPin(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin)

	
	//Declare PA2  as input pull up pin
	//GPIO_Init (GPIOA, GPIO_PIN_2, GPIO_MODE_IN_PU_IT);

	//Declare PB5 as push pull Output pin
  //GPIO_Init (GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

  //GPIO_Init (GPIOC, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);

	//GPIO_WriteHigh(GPIOC, GPIO_PIN_3); //LED ON
	
  while(1){
	//GPIO_WriteReverse(GPIOB,GPIO_PIN_5);
	//GPIO_WriteLow(GPIOC, GPIO_PIN_3); //LED OFF
	spiEnableReceiveMode(channel, NSS2);
	DelayMs(500);
	//GPIO_WriteHigh(GPIOC, GPIO_PIN_3); //LED ON

	spiWriteKey(0xe0, channel , NSS);
	DelayMs(20);	
	spiReadreg(48, NSS2); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
	spiReadreg(52, NSS2); //read fifo status register		(id auto-ack=on then, if bits 0-5 are zero then the ack sequence was completed corretly
	spiReadreg(50, NSS2); //read received data (first byte = number of byte received (RegH))	
	DelayMs(480);

	}

}


#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif