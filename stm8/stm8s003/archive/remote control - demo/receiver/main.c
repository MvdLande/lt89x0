#include "stm8s.h"
#include "stm8s_gpio.h"
#include "stm8s_spi.h"
//#include "stm8s_i2c.h"
#include "stm8s_tim4.h"
#include "spi.h"
#include "lt89x0.h"
#include "MAX7219.h"
#include "stm8s_clk.h"
#include "scheduler.h"

#define MAX7219 NSS2
#define LT8920	NSS


void DelayMs (uint16_t ms){ //Function Definition
  uint16_t i =0 ;
  uint8_t j=0;
  for (i=0; i<=ms; i++)
  {
    for (j=0; j<120; j++) // Nop = Fosc/4
      _asm("nop"); //Perform no operation //assembly code              
  }
}

Delay10us(void)
{
	uint8_t j=0;
	for (j=0; j<12; j++) // Nop = Fosc/4
		_asm("nop"); //Perform no operation //assembly code              
}

void DeInit_GPIO(void)
{
	GPIO_DeInit(GPIOA); // prepare Port A for working
	GPIO_DeInit(GPIOB); // prepare Port B for working
	GPIO_DeInit(GPIOC); // prepare Port C for working
	GPIO_DeInit(GPIOD); // prepare Port D for working	
}

#define stop_button	 	GPIOB,GPIO_PIN_4 	/*SCL (pullup resistor required)*/
//#define walk_button	 	GPIOA,GPIO_PIN_2	// PA2 (used on test board without LT8920)
#define walk_button 	GPIOB,GPIO_PIN_5 	/*SDA (pullup resistor required)*/ 
#define standby_button GPIOD,GPIO_PIN_4 	/*PD4 */

#define WALK_FLAG 0x01
#define STOP_FLAG 0x02
#define STANDBY_FLAG 0x04

#define UPPER_MASK 0xf0
#define LOWER_MASK 0x0f
#define DebounceDelay 10

uint8_t	key_status = 0xff;
uint8_t channel = 100;

#define TRANSMIT 1

void initKeys(void)
{
	GPIO_Init(walk_button,  GPIO_MODE_IN_PU_NO_IT);
	GPIO_Init(stop_button,  GPIO_MODE_IN_PU_NO_IT);
	GPIO_Init(standby_button,  GPIO_MODE_IN_PU_NO_IT);
}

//volatile uint8_t Debounce1, Debounce2, Debounce3;	// debounce counters

void KeyScanTask(void){
}

void StateMachineTask(void){
}

enum ComState{Idle, Transmit};
#define MAXRETRY 4

void CommunicationTask(void){
	if( GPIO_ReadInputPin(PKT) != 0x00){
		lt89x0_ReadReg(48, LT8920); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
		if (RegH & 0x80){ // CRC error
			lt89x0_EnableReceiveMode(channel, LT8920);
			max7219_write(MAX7219_Digit0, 0, MAX7219 );
			return;
		}
		else{ // read data
			lt89x0_ReadReg(50, LT8920); //read received data (first byte = number of byte received (RegH))
			lt89x0_EnableReceiveMode(channel, LT8920);
			key_status = RegL;
			max7219_write(MAX7219_Digit0, 1, MAX7219 );
		}
	}
}

void DisplayTask(void){
	static uint8_t prev_key_status = 0;
	if (prev_key_status != key_status){
		if (key_status & WALK_FLAG)
			max7219_write(MAX7219_Digit7, 0, MAX7219 );
		else	
			max7219_write(MAX7219_Digit7, 1, MAX7219 );
		
		if (key_status & STOP_FLAG)
			max7219_write(MAX7219_Digit6, 0, MAX7219 );
		else	
			max7219_write(MAX7219_Digit6, 1, MAX7219 );
		
		if (key_status & STANDBY_FLAG)
			max7219_write(MAX7219_Digit5, 0, MAX7219 );
		else	
			max7219_write(MAX7219_Digit5, 1, MAX7219 );

		prev_key_status = key_status;
	}
}




void main(){
	uint8_t i=0;
	spi_Init();
	lt89x0_Init();
	lt89x0_EnableReceiveMode(channel, LT8920);
	max7219_CodeB_init(MAX7219);
	//max7219_init(MAX7219);	
	//initKeys();
	//CLK_DeInit();
	//TIM4_DeInit();
	//CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV8);
	//TIM4_PrescalerConfig(TIM4_PRESCALER_128, TIM4_PSCRELOADMODE_IMMEDIATE);
	InitTasks();
	
	TIM4_TimeBaseInit(TIM4_PRESCALER_128, 156) ; //fclk = 2MHz, 10ms interval
	TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE); 

	
	// prio 0 has the highest priority
	CreateTask (&KeyScanTask, 0, 1, 0, TaskPeriodic);	// run task every 1 systicks
	CreateTask (&StateMachineTask, 1, 1, 0, TaskPeriodic);	// run task every 1 systicks
	CreateTask (&CommunicationTask, 2, 1, 0, TaskPeriodic);	// run task every 1 systicks
	CreateTask (&DisplayTask, 3, 1, 0, TaskPeriodic);	// run task every 1 systicks
	EnableTask (0);
	EnableTask (1);	
	EnableTask (2);		
	EnableTask (3);		

	TIM4_Cmd(ENABLE);
	enableInterrupts();

	//max7219_write(MAX7219_Digit0, HEX_To_7SEG[0x0E], MAX7219); 
	//max7219_write(MAX7219_Digit1, SEG_CHAR_r, MAX7219); 
	//max7219_write(MAX7219_Digit2, SEG_CHAR_r, MAX7219); 
	//max7219_write(MAX7219_Digit3, SEG_CHAR_o, MAX7219); 
	//max7219_write(MAX7219_Digit4, SEG_CHAR_r + SEG_DP, MAX7219); 
	//max7219_write(MAX7219_Digit5, SEG_DP, MAX7219); 
	
	

	//lt89x0_ReadReg(7, NSS);
	//lt89x0_ReadReg(7, NSS2);
	//lt89x0_EnableReceiveMode(channel,NSS2);
	//lt89x0_TransmitByte(0xe0, channel, LT8920);
	//lt89x0_ReadReg(48, NSS2); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
	//lt89x0_ReadReg(50, NSS2); //read received data
	
  while(1){
		i++;
		//lt89x0_EnableReceiveMode(channel, NSS2);
		DelayMs(500);

		//lt89x0_TransmitByte(i, channel , LT8920);
		DelayMs(100);	
		//lt89x0_ReadReg(48, NSS2); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
		//lt89x0_ReadReg(52, LT8920); //read fifo status register		(id auto-ack=on then, if bits 0-5 are zero then the ack sequence was completed corretly
		//lt89x0_ReadReg(50, NSS2); //read received data (first byte = number of byte received (RegH))	
		DelayMs(400);
		//max7219_write(MAX7219_Digit7, HEX_To_7SEG[i & 0x0F], MAX7219); 
	}
}


#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif