#include "stm8s.h"
#define FIFODepth 4			// buffer size
#define FIFOMask 0x03		// address bitmask

typedef struct FIFO{
	volatile uint8_t RP;	// Read pointer, points to last address written
	volatile uint8_t WP;	// Write pointer, points to last address read
	volatile uint8_t Buffer[FIFODepth];
} FIFO;

uint8_t FIFOCheckFreeSpace(FIFO *fifo){
	if(fifo->WP >= fifo->RP)
		return FIFODepth - (fifo->WP - fifo->RP);
	else
		return fifo->RP - fifo->WP;
}