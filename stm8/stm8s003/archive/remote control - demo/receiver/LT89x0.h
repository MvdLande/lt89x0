#include "stm8s.h"
#include "stm8s_gpio.h"
#include "spi.h"

extern uint8_t Status, RegH, RegL;

//#define SecondLT8920

//LT8920 Chip Enable (nRESET) pin definitions
#define CE	 	GPIOA,GPIO_PIN_1 	/*PA1*/ 
#define CE2	 	GPIOB,GPIO_PIN_4 	/*SCL (pullup resistor required)*/

//LT8920 PKT interrupt pin definitions
#define PKT 	GPIOA,GPIO_PIN_2 	/*PA2*/
#define PKT2 	GPIOB,GPIO_PIN_5 	/*SDA (pullup resistor required)*/ 


//----------------------------------------------------------------------------
void lt89x0_ReadReg(uint8_t reg, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_WriteReg(uint8_t reg, uint8_t byteH, uint8_t byteL, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_TransmitByte(uint8_t Byte, uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_EnableReceiveMode(uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_WriteConfig(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_Init(void);

