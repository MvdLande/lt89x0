#include "stm8s.h"
#include "common.h"


#define WALK_TIME (10000 / TICKPERIOD) /*Walk signal is 10 seconds ACTIVE */
#define TRANSITION_TIME (1000 / TICKPERIOD) 
#define AUTOWAIT_TIME (10000 / TICKPERIOD) 
//#define WALK_TIME 10 /*Walk signal is 10 seconds ACTIVE */
//#define TRANSITION_TIME 4 


static uint8_t update_state(uint8_t display_state);
static void update_leds(uint8_t display_state);

enum DISPLAY_STATE
{
	PEDESTRIAN_RED, /* car_green */
	CAR_YELLOW, /* pedestrian_red */
	CAR_RED,  /*pedestrian_red */ 
	PEDESTRIAN_GREEN, /* CAR_RED */
	LEDS_OFF
};

enum OPERATION_MODE
{
	ACTIVE,
	WALK_REQUEST,
	WALK_REQUEST_END,
	BLINK,
	GO_REQUEST,
	STOP_REQUEST,
	AUTO_REQUEST,
	AUTO_REQUEST_END,
	AUTO_REQUEST_WAIT
};

void statemachine_task(void)
{
	static uint8_t display_state = PEDESTRIAN_RED;
	display_state = update_state(display_state);
	update_leds(display_state);
}

static uint8_t update_state(uint8_t current_state)
{
	static uint8_t mode = ACTIVE;
	static uint16_t timestamp = 1;
	uint8_t flags;
	uint8_t display_state;
	
	display_state = current_state;
	flags = scheduler_getVar(FLAGS_HANDLE);

	// *******************************************************
	// process flags
	// *******************************************************
	if(flags && WALK_FLAG)
	{
		// update mode
		mode = WALK_REQUEST;
		flags = flags & ~WALK_FLAG; // clear flag
		scheduler_setVar(FLAGS_HANDLE, flags);
		timestamp = scheduler_getTime();
		// update display_state
		display_state = PEDESTRIAN_RED;
	}
	
	if(flags && BLINK_FLAG)
	{
		flags = flags & ~BLINK_FLAG; // clear flag
		scheduler_setVar(FLAGS_HANDLE, flags);		
		if (BLINK == mode)
		{
			//  update mode
			mode = ACTIVE; // exit BLINK mode
			// update display_state
			display_state = PEDESTRIAN_RED;
		}
		else
		{
			// update mode
			mode = BLINK; // enter BLINK mode
			timestamp = scheduler_getTime();
			// update display_state
			display_state  = CAR_YELLOW;
		}
	}

	if(flags && GO_FLAG)
	{
		// update mode
		mode = GO_REQUEST; // Keep this output untill another request is received
		flags = flags & ~GO_FLAG; // clear flag
		scheduler_setVar(FLAGS_HANDLE, flags);
		timestamp = scheduler_getTime();
		// update display_state
		display_state = PEDESTRIAN_RED;
	}

	if(flags && STOP_FLAG)
	{
		// update mode
		mode = STOP_REQUEST; // Keep this output untill another request is received
		flags = flags & ~STOP_FLAG; // clear flag
		scheduler_setVar(FLAGS_HANDLE, flags);
		timestamp = scheduler_getTime();
		// update display_state
		display_state = PEDESTRIAN_GREEN;
	}
	
	if(flags && AUTO_FLAG)
	{
		// update mode
		mode = AUTO_REQUEST; // no function assigned yet.
		flags = flags & ~AUTO_FLAG; // clear flag
		scheduler_setVar(FLAGS_HANDLE, flags);
		timestamp = scheduler_getTime();
		// update display_state
		display_state = PEDESTRIAN_RED;
	}	
	
	// *******************************************************
	// process modes, modify display_state
	// *******************************************************
	if(BLINK == mode)
	{
		if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
		{
			if (CAR_YELLOW == display_state){
				display_state = LEDS_OFF;
				timestamp = scheduler_getTime();
			}
			else
			{
				display_state = CAR_YELLOW;
				timestamp = scheduler_getTime();
			}
		}
		//return display_state;
	}
	
	if((WALK_REQUEST == mode) || (AUTO_REQUEST == mode))
	{
		if(PEDESTRIAN_RED == display_state)
		{
			if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
			{
				display_state = CAR_YELLOW;
				timestamp = scheduler_getTime();
			}
		}

		if(CAR_YELLOW == display_state)
		{
			if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
			{
				display_state = CAR_RED;
				timestamp = scheduler_getTime();
			}
		}	
		
		if(CAR_RED == display_state)
		{
			if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
			{
				display_state = PEDESTRIAN_GREEN;
				timestamp = scheduler_getTime();
			}
		}	
		
		if(PEDESTRIAN_GREEN == display_state)
		{
			if(scheduler_getTimeElapsed(timestamp) > WALK_TIME)
			{
				display_state = CAR_YELLOW;
				timestamp = scheduler_getTime();
				if(WALK_REQUEST == mode)
				{
					mode = WALK_REQUEST_END;
				}
				else
				{
					mode = AUTO_REQUEST_END;
				}
			
			}
		}
		//return display_state;		
	}

	if((WALK_REQUEST_END == mode) || (AUTO_REQUEST_END == mode))
	{
		if(PEDESTRIAN_GREEN == display_state)
		{
			if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
			{
				display_state = CAR_RED;
				timestamp = scheduler_getTime();
			}
		}

		if(CAR_RED == display_state)
		{
			if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
			{
				display_state = CAR_YELLOW;
				timestamp = scheduler_getTime();
			}
		}

		if(CAR_YELLOW == display_state)
		{
			if(scheduler_getTimeElapsed(timestamp) > TRANSITION_TIME)
			{
				display_state = PEDESTRIAN_RED;
				timestamp = scheduler_getTime();
				if(WALK_REQUEST_END == mode)
				{
					mode = ACTIVE;
				}
				else
				{
					mode = AUTO_REQUEST_WAIT;					
				}
			}
		}	
	}

	if(AUTO_REQUEST_WAIT == mode)
	{
		if(scheduler_getTimeElapsed(timestamp) > AUTOWAIT_TIME)
		{
			display_state = PEDESTRIAN_RED;
			timestamp = scheduler_getTime();
			mode = AUTO_REQUEST;					
		}
	}	

	return display_state;	
}



static void update_leds(uint8_t display_state)
{
	if(display_state == PEDESTRIAN_RED)
	{
		GPIO_WriteHigh(CAR_G);		
		GPIO_WriteLow(CAR_Y);	
		GPIO_WriteLow(CAR_R);	
		GPIO_WriteLow(PED_G);		
		GPIO_WriteHigh(PED_R);	
	}
	if(display_state == CAR_YELLOW)
	{
		GPIO_WriteLow(CAR_G);		
		GPIO_WriteHigh(CAR_Y);	
		GPIO_WriteLow(CAR_R);	
		GPIO_WriteLow(PED_G);		
		GPIO_WriteHigh(PED_R);	
	}
	if(display_state == CAR_RED)
	{
		GPIO_WriteLow(CAR_G);		
		GPIO_WriteLow(CAR_Y);	
		GPIO_WriteHigh(CAR_R);	
		GPIO_WriteLow(PED_G);		
		GPIO_WriteHigh(PED_R);	
	}

	if(display_state == PEDESTRIAN_GREEN)
	{
		GPIO_WriteLow(CAR_G);		
		GPIO_WriteLow(CAR_Y);	
		GPIO_WriteHigh(CAR_R);	
		GPIO_WriteHigh(PED_G);		
		GPIO_WriteLow(PED_R);	
	}
	if(display_state == LEDS_OFF)
	{
		GPIO_WriteLow(CAR_G);		
		GPIO_WriteLow(CAR_Y);	
		GPIO_WriteLow(CAR_R);	
		GPIO_WriteLow(PED_G);		
		GPIO_WriteLow(PED_R);	
	}
}