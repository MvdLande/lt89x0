#include "stm8s.h"
#include "stm8s_gpio.h"
#include "common.h"
#include "lt89x0.h"

#define DEFAULT_CHANNEL 100
#define CRC_MASK 0x80
#define STATUS_REGISTER 48
#define TXRX_FIFO_REG 50
volatile uint8_t channel = DEFAULT_CHANNEL;
/*! Communication task function
 *
 *	Be aware of the following: The "real-time" scheduler used in this project
 *  runs all tasks in interrupt contect. Therefore it is assumes that a "task 
 *  function" does not contain an infinite loop. 
 *  Instead, it assumes that the task is just a function which exits when the 
 *  work is done. The task may not be put to "sleep" for a certain amount
 *  of time or loop indefinitely. Instead, the function must exit. 
 *  The "real-time" scheduler takes care of (periodically) activating the task
 *  function. 
 *  If some state must be preserved between multiple activations, then a local
 *  variable may be declared as "static" 
 *  
 */

void communication_task(void)
{
	if( GPIO_ReadInputPin(PKT) != 0x00)
	{
		lt89x0_ReadReg(STATUS_REGISTER, LT8920); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
		if (RegH & CRC_MASK) // CRC error
		{
			lt89x0_EnableReceiveMode(channel, LT8920);
			return;
		}
		else // read data
		{
			lt89x0_ReadReg(TXRX_FIFO_REG, LT8920); //read received data (first byte = number of byte received (RegH))
			scheduler_setVar(FLAGS_HANDLE, RegL);
			lt89x0_EnableReceiveMode(channel, LT8920);
		}
	}
}
