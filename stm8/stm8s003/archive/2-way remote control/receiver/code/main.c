#include "stm8s.h"
#include "stm8s_gpio.h"
#include "stm8s_spi.h"
//#include "stm8s_i2c.h"
#include "stm8s_tim4.h"
#include "spi.h"
#include "lt89x0.h"
#include "MAX7219.h"
#include "stm8s_clk.h"
#include "io.h"
#include "KeyscanTask.h"
#include "CommunicationTask.h"
#include "statemachineTask.h"
#include "common.h"



void DelayMs (uint16_t ms){ //Function Definition
  uint16_t i =0 ;
  uint8_t j=0;
  for (i=0; i<=ms; i++)
  {
    for (j=0; j<120; j++) // Nop = Fosc/4
      _asm("nop"); //Perform no operation //assembly code              
  }
}

Delay10us(void)
{
	uint8_t j=0;
	for (j=0; j<12; j++) // Nop = Fosc/4
		_asm("nop"); //Perform no operation //assembly code              
}

void DeInit_GPIO(void)
{
	GPIO_DeInit(GPIOA); // prepare Port A for working
	GPIO_DeInit(GPIOB); // prepare Port B for working
	GPIO_DeInit(GPIOC); // prepare Port C for working
	GPIO_DeInit(GPIOD); // prepare Port D for working	
}



//#define UPPER_MASK 0xf0
//#define LOWER_MASK 0x0f



//#define TRANSMIT 1

//volatile static uint8_t Flags=0;


//volatile uint8_t Debounce1, Debounce2, Debounce3;	// debounce counters



//enum ComState{Idle, Transmit};
//#define MAXRETRY 4



void main(){
	static uint32_t i=0;
	spi_Init();
	lt89x0_Init();
	lt89x0_EnableReceiveMode(channel, LT8920);
	//max7219_CodeB_init(MAX7219);
  IO_Init();	
	// init shared variables
	scheduler_setVar(FLAGS_HANDLE, 0x00);
	init_keys();
	//max7219_init(MAX7219);	
	//initKeys();
	//CLK_DeInit();
	//TIM4_DeInit();
	//CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV8);
	//TIM4_PrescalerConfig(TIM4_PRESCALER_128, TIM4_PSCRELOADMODE_IMMEDIATE);
	scheduler_initTasks();
	// prio 0 has the highest priority
	scheduler_createTask (&keyscan_task, PRIO0, 2, 0, TASK_PERIODIC);	// run task every 2 systicks
	scheduler_createTask (&statemachine_task, PRIO1, 1, 0, TASK_PERIODIC);	// run task every 1 systicks
	scheduler_createTask (&communication_task, PRIO2, 1, 0, TASK_PERIODIC);	// run task every 1 systicks
	//CreateTask (&DisplayTask, PRIO3, 1, 0, TASK_PERIODIC);	// run task every 1 systicks
	
	// Task number equals task priority
	scheduler_enableTask (PRIO0);
	scheduler_enableTask (PRIO1);	
	scheduler_enableTask (PRIO2);		
	//scheduler_enableTask (PRIO3);		
	//scheduler_start();
	//scheduler_setVar(FLAGS_HANDLE, 0x00);


	//max7219_write(MAX7219_Digit0, HEX_To_7SEG[0x0E], MAX7219); 
	//max7219_write(MAX7219_Digit1, SEG_CHAR_r, MAX7219); 
	//max7219_write(MAX7219_Digit2, SEG_CHAR_r, MAX7219); 
	//max7219_write(MAX7219_Digit3, SEG_CHAR_o, MAX7219); 
	//max7219_write(MAX7219_Digit4, SEG_CHAR_r + SEG_DP, MAX7219); 
	//max7219_write(MAX7219_Digit5, SEG_DP, MAX7219); 
	
	

	//lt89x0_ReadReg(7, NSS);
	//lt89x0_ReadReg(7, NSS2);
	//lt89x0_EnableReceiveMode(channel,NSS2);
	//lt89x0_TransmitByte(0xe0, channel, LT8920);
	//lt89x0_ReadReg(48, NSS2); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
	//lt89x0_ReadReg(50, NSS2); //read received data
	
  while(1){
		i++;
		if(i%50 == 0)
		{
			i++;
		}
		//lt89x0_EnableReceiveMode(channel, NSS2);
		//DelayMs(500);

		//lt89x0_TransmitByte(i, channel , LT8920);
		//DelayMs(100);	
		//lt89x0_ReadReg(48, NSS2); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
		//lt89x0_ReadReg(52, LT8920); //read fifo status register		(id auto-ack=on then, if bits 0-5 are zero then the ack sequence was completed corretly
		//lt89x0_ReadReg(50, NSS2); //read received data (first byte = number of byte received (RegH))	
		//DelayMs(400);
		//max7219_write(MAX7219_Digit7, HEX_To_7SEG[i & 0x0F], MAX7219); 
	}
}


#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif