#include "scheduler.h"
#include "spi.h"

static task_t tasklist[NUMTASKS];
static uint8_t vars[NUMVARS];
static uint16_t ticks;

#define MAXTICKS 0xFFFF

static void scheduler_checkPendingTasks (void);

static void scheduler_runPendingTasks (void);


void scheduler_setVar(uint8_t var_handle, uint8_t value)
{
	if (var_handle < NUMVARS)
	{
		scheduler_enterCriticalSection();
		vars[var_handle] = value;
		scheduler_leaveCriticalSection();
	}
}

uint8_t scheduler_getVar(uint8_t var_handle)
{
	if (var_handle < NUMVARS)
	{
		return vars[var_handle];
	}
}

void scheduler_start (void)
{
	TIM4_TimeBaseInit(TIM4_PRESCALER_128, 156) ; //fclk = 2MHz, 10ms interval
	TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE); 
	enableInterrupts();
	TIM4_Cmd(ENABLE);
}

void scheduler_stop (void)
{
	TIM4_ITConfig(TIM4_IT_UPDATE, DISABLE); 
}

void scheduler_initTasks(void){
	uint8_t i;
	task_t * task;
	for (i=0;i<NUMTASKS;i++){
		task = &tasklist[i];	
		task->task_status = 0x00;
	}
}

uint16_t scheduler_getTimeElapsed(uint16_t timestamp)
{
	uint16_t dif;
	if (ticks >= timestamp)
	{
		dif = (ticks - timestamp);
	}
	else
	{
		dif = (MAXTICKS - timestamp + ticks);
	}
	return dif;	
}
uint16_t scheduler_getTime(void)
{
	return ticks;
}

void scheduler_createTask (void (*task_function) (void), uint8_t task_priority, uint16_t task_period, uint16_t task_phase, uint8_t task_properties)
{
	task_t * task = &tasklist[task_priority];
	task->task_period = task_period;
	task->ticks_remaining = task_phase;
	task->task_function = task_function;
	task->task_status = task_properties | TASK_INITIALIZED;
}

void scheduler_enableTask (uint8_t task_priority)
{
	task_t * task = &tasklist[task_priority];
	task->task_status |= TASK_ENABLED;
}

void scheduler_disableTask (uint8_t task_priority)
{
	task_t * t = &tasklist[task_priority];
	t->task_status &= ~TASK_ENABLED;
}

@far @interrupt void SysTick (void)
{
  static uint16_t count;
	// uninterruptable part
	if(ticks<0xFFFF)
	{
		ticks++; // increment ticks counter, this counter is used in "time elapsed" (delay) functions
	}
	else
	{
		ticks = 0;
	}
	scheduler_checkPendingTasks();
	TIM4_ClearITPendingBit(TIM4_IT_UPDATE);	
	// interruptable part
	scheduler_runPendingTasks();
}


static void scheduler_checkPendingTasks (void)
{
	uint8_t i; 
	task_t * task;
	for (i=0;i<NUMTASKS;i++)
	{
    task = &tasklist[i];
		if (task->task_status & TASK_ENABLED) // countdown
		{
      if (0 == task->ticks_remaining--)
			{
				if (task->task_status & TASK_PERIODIC)
				{
					task->ticks_remaining = task->task_period-1; 	// load next activation period
				}
				else	
				{
					task->task_status &= ~TASK_ENABLED;  //disable task
				}
				task->task_status |= TASK_PENDING;	// set Pending flag
			}
		}
	}		
}

static void scheduler_runPendingTasks (void)
{
	// activated tasks can be interrupted by pending higher priority tasks 
  uint8_t i = NUMTASKS-1; 
	task_t * task;
  for (i=0;i<NUMTASKS;i++)
	{
    task = &tasklist[i];
    if (task->task_status & TASK_PENDING)
		{	
			if (task->task_status & TASK_ACTIVATED)
			{
				// task has already been activated (running)
				// do not activate (start) it again			
				break;	
			}
			task->task_status &= ~TASK_PENDING;	// clear Pending flag				
			task->task_status |= TASK_ACTIVATED; // set Activated flag
			task->task_function();							// run the task function
			task->task_status &= ~TASK_ACTIVATED;// clear Activated flag
		}
  }
}


