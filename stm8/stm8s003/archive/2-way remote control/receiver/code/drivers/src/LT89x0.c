
#include "LT89x0.h"
#include "delay.h"
#include "scheduler.h"

#define WRITE		0x7F
#define READ		0x80
uint8_t Status;
uint8_t RegH;
uint8_t RegL;

//----------------------------------------------------------------------------
void lt89x0_ReadReg(uint8_t reg, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
	// claim SPI bus
	scheduler_enterCriticalSection();
	GPIO_WriteLow(SPI_NSS_port, SPI_NSS_pin);
	Status = spi_ReadWrite(READ | reg);
  RegH = spi_ReadWrite(0x00);
  RegL = spi_ReadWrite(0x00);
	GPIO_WriteHigh(SPI_NSS_port, SPI_NSS_pin);
	scheduler_leaveCriticalSection();
	// free SPI bus
}

//----------------------------------------------------------------------------


void lt89x0_WriteReg(uint8_t reg, uint8_t byteH, uint8_t byteL, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
	// claim SPI bus
	scheduler_enterCriticalSection();
	GPIO_WriteLow(SPI_NSS_port, SPI_NSS_pin);
	spi_ReadWrite(WRITE & reg);
  spi_ReadWrite(byteH);
  spi_ReadWrite(byteL);
	GPIO_WriteHigh(SPI_NSS_port, SPI_NSS_pin);
	scheduler_leaveCriticalSection();
	// free SPI bus
}


void lt89x0_TransmitByte(uint8_t Byte, uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
  lt89x0_WriteReg(7, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin); /* disable send and receive mode */
	DelayMs (3);
  lt89x0_WriteReg(52, 0x80, 0x00, SPI_NSS_port, SPI_NSS_pin); /* clear transmit buffer */
	/* construct packet, first byte is packet size */	
  lt89x0_WriteReg(50, 0x01, Byte, SPI_NSS_port, SPI_NSS_pin); /* Packet size = One byte */
	//lt89x0_WriteReg(7, 0x01, 0x30, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(7, 0x01, channel & 0xef, SPI_NSS_port, SPI_NSS_pin); /* start transmission on channel, frequency = 2402 + channel MHz */
}

void lt89x0_EnableReceiveMode(uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
  lt89x0_WriteReg(7, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin); /* disable send and receive mode */
	DelayMs (3);
  lt89x0_WriteReg(52, 0x00, 0x80, SPI_NSS_port, SPI_NSS_pin); /* clear receive buffer */
	//lt89x0_WriteReg(7, 0x00, 0xB0, SPI_NSS_port, SPI_NSS_pin); //enable receive mode, select channel 30
  lt89x0_WriteReg(7, 0x00, 0x80 | (channel & 0xef), SPI_NSS_port, SPI_NSS_pin); /* enable receive mode, set channel No., frequency = 2402 + channel MHz */
}

void lt89x0_WriteConfig(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin )
{
  lt89x0_WriteReg(0, 0x6f, 0xe0, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(1, 0x56, 0x81, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(2, 0x66, 0x17, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(4, 0x9c, 0xc9, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(5, 0x66, 0x37, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(7, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(8, 0x6c, 0x90, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(9, 0x48, 0x00, SPI_NSS_port, SPI_NSS_pin);				// 5.5dBm
  lt89x0_WriteReg(10, 0x7f, 0xfd, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(11, 0x00, 0x08, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(12, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(13, 0x48, 0xbd, SPI_NSS_port, SPI_NSS_pin);

  lt89x0_WriteReg(22, 0x00, 0xff, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(23, 0x80, 0x05, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(24, 0x00, 0x67, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(25, 0x16, 0x59, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(26, 0x19, 0xe0, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(27, 0x13, 0x00, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(28, 0x18, 0x00, SPI_NSS_port, SPI_NSS_pin);

  lt89x0_WriteReg(32, 0x50, 0x00, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(33, 0x3f, 0xc7, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(34, 0x20, 0x00, SPI_NSS_port, SPI_NSS_pin);
  //lt89x0_WriteReg(35, 0x03, 0x00, SPI_NSS_port, SPI_NSS_pin); // max 3 re-transmissions when auto-ack=on
  lt89x0_WriteReg(35, 0x0F, 0x00, SPI_NSS_port, SPI_NSS_pin); // max 15 re-transmissions when auto-ack=on
  lt89x0_WriteReg(36, 0x03, 0x80, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(37, 0x03, 0x80, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(38, 0x5a, 0x5a, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(39, 0x03, 0x80, SPI_NSS_port, SPI_NSS_pin);
  //lt89x0_WriteReg(40, 0x44, 0x07, SPI_NSS_port, SPI_NSS_pin); // allow 7-1=6 syncword bit errors
  lt89x0_WriteReg(40, 0x44, 0x01, SPI_NSS_port, SPI_NSS_pin); // allow 1-1=0 syncword bit errors  
  //lt89x0_WriteReg(41, 0xB0, 0x00, SPI_NSS_port, SPI_NSS_pin);  //crc on scramble off ,1st byte packet length ,auto ack off, PKT active high
	lt89x0_WriteReg(41, 0xB8, 0x00, SPI_NSS_port, SPI_NSS_pin); //crc on, scramble off, 1st byte = packet length, auto ack on, PKT active high
  lt89x0_WriteReg(42, 0xfb, 0xb0, SPI_NSS_port, SPI_NSS_pin);  //
  lt89x0_WriteReg(43, 0x00, 0x0f, SPI_NSS_port, SPI_NSS_pin);
  lt89x0_WriteReg(44, 0x01, 0x00, SPI_NSS_port, SPI_NSS_pin); // set datarate = 1Mbps 
  lt89x0_WriteReg(45, 0x00, 0x80, SPI_NSS_port, SPI_NSS_pin); // Best value is 0080H when data rate is 1Mbps
  //lt89x0_WriteReg(44, 0x04, 0x00, SPI_NSS_port, SPI_NSS_pin); // set datarate = 256Kbps 
  //lt89x0_WriteReg(45, 0x05, 0x52, SPI_NSS_port, SPI_NSS_pin); // Best value is 0552H when data rate is not 1Mbps
	
  lt89x0_WriteReg(50, 0x00, 0x00, SPI_NSS_port, SPI_NSS_pin);
	DelayMs(200);

  lt89x0_WriteReg(7, 0x01, 0x00, SPI_NSS_port, SPI_NSS_pin); /* start transmission on channel 0, frequency = 2402 + 0 = 2.402GHz */
	DelayMs(2);
  lt89x0_WriteReg(7, 0x00, 0x30, SPI_NSS_port, SPI_NSS_pin); /* disable send and receive mode, set channel = 0x30 (48) */
	
}

//----------------------------------------------------------------------------
void lt89x0_Init(void)
{
	GPIO_Init(CE, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(PKT, GPIO_MODE_IN_PU_NO_IT);
	GPIO_WriteLow(CE);	
  DelayMs(100);
	GPIO_WriteHigh(CE);	
  DelayMs(200);
	lt89x0_WriteConfig(NSS);
	
#ifdef SecondLT8920
	GPIO_Init(CE2, GPIO_MODE_OUT_PP_HIGH_SLOW);	
	GPIO_Init(PKT2, GPIO_MODE_IN_PU_NO_IT);
	GPIO_WriteLow(CE2);	
	DelayMs(100);
	GPIO_WriteHigh(CE2);		
	DelayMs(200);
	lt89x0_WriteConfig(NSS2);
#endif

}
