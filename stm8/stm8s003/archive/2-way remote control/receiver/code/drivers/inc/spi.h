/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPI_H
#define __SPI_H

#include "stm8s.h"
#include "stm8s_gpio.h"

// (soft) SPI pin definitions (2 slaves)
#define NSS	 	GPIOA,GPIO_PIN_3 	/*PA3*/ 
#define NSS2 	GPIOD,GPIO_PIN_5 	/*PD5*/ 
#define MISO 	GPIOC,GPIO_PIN_7 	/*PC7*/
#define MOSI 	GPIOC,GPIO_PIN_6 	/*PC6*/
#define SCK 	GPIOC,GPIO_PIN_5 	/*PC5*/
void spi_Init(void);
uint8_t spi_ReadWrite(uint8_t Byte);

#endif
