
#include "spi.h"
#include "delay.h"

//-----------------------------------------------------------------------------
uint8_t spi_ReadWrite(uint8_t Byte)
{
	uint8_t i;

   for (i = 0; i < 8; i++)
   {
      if((Byte & 0x80) != 0x00)
				GPIO_WriteHigh(MOSI);	
			else
				GPIO_WriteLow(MOSI);	
			Delay10us();
			GPIO_WriteHigh(SCK);
			Delay10us();
      Byte <<= 1;
      if( GPIO_ReadInputPin(MISO) != 0x00)			
				Byte |= 0x01;
			GPIO_WriteLow(SCK);
    }
	return (Byte);
}

//----------------------------------------------------------------------------
void spi_Init(void)
{
	GPIO_Init(NSS, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(NSS2, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(MISO,  GPIO_MODE_IN_PU_NO_IT);
	GPIO_Init(MOSI, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_Init(SCK, GPIO_MODE_OUT_PP_HIGH_SLOW);
	GPIO_WriteHigh(NSS);	
	GPIO_WriteHigh(NSS2);	
	GPIO_WriteLow(SCK);		
}
