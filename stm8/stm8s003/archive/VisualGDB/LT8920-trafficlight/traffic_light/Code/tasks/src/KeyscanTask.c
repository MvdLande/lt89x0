#include "stm8s_gpio.h"
#include "IO.h" 
#include "keypress_filter.h"
#include "common.h"


#define SW_WALK_INACTIVE SW_WALK_pin /* the default level is high, (unpressed/inactive) */ 
#define T_MIN 60/TICKPERIOD 		/* 60ms/10ms */
#define T_FUNCTION1_MAX 1000/TICKPERIOD   /*1000ms/10ms*/
#define T_FUNCTION2 3000/TICKPERIOD 

//enum EVENT_TYPE{
//	NO_EVENT, 
//	KEY_RELEASED
//};

volatile static keypress_filter_state_t sw_walk_key;
#ifdef TRANSMIT_MODE
volatile static keypress_filter_state_t sw_go_key;	
volatile static keypress_filter_state_t sw_stop_key;
volatile static keypress_filter_state_t sw_blink_key;
volatile static keypress_filter_state_t sw_auto_key;
#endif

void init_keys(void)
{
	keypress_filter_init(&sw_walk_key, TRUE); // inverted input
#ifdef TRANSMIT_MODE
	keypress_filter_init(&sw_go_key, TRUE); // inverted input
	keypress_filter_init(&sw_stop_key, TRUE); // inverted input
	keypress_filter_init(&sw_blink_key, TRUE); // inverted input
	keypress_filter_init(&sw_auto_key, TRUE); // inverted input
#endif
}


void scan_receiver_keys(void);
void scan_transmitter_keys(void);

void keyscan_task(void)
{
#ifndef TRANSMIT_MODE
	scan_receiver_keys();
#else
	scan_transmitter_keys();
#endif	
}

void scan_receiver_keys(void)
{
#ifndef TRANSMIT_MODE
	// 10ms period
	// Currently. there is only one key named "SW"
	// "t" is the time that key SW is active (pressed)
	// if t=100ms then the pedestrian requests to cross the street and the 
	// "WALK" sequence is requested.
	// if t=3s then the "STANDBY" sequence is requested, this means that
	// the CAR_Yellow and the Pedestrian_RED light blink at f=0.5Hz
	// Glitches (key released) < 100ms will be filtered out.
	// default key state = inactive
  // task period=20ms
	static uint8_t flags;
	// check for level changes (key presses, glitches etc.)
	// filter out glitches
	keypress_filter(GPIO_ReadInputPin(SW_WALK), &sw_walk_key);
	// key logic for two function
	// 1) pedestrian requests to cross the street
	//		t < t_function1_max
	// 2) enter standby state (blink yellow light/red walk sigh)
	//		t > t_function2
	flags = scheduler_getVar(FLAGS_HANDLE);	
	// process event based actions
	if (KEY_ACTIVATED == sw_walk_key.event)
	{
		//if (sw_walk_key.level_stable_tick_counter < T_FUNCTION1_MAX)
		//{
			// add function 1 code
			flags |= WALK_FLAG;
			scheduler_setVar(FLAGS_HANDLE, flags);
		//}
		sw_walk_key.event = NO_EVENT;
	}
	// process time based actions
	if (1 == sw_walk_key.output)
	{	
		if (T_FUNCTION2 == sw_walk_key.level_stable_tick_counter)
		{
			// add function 2 code
			flags |= BLINK_FLAG;
			scheduler_setVar(FLAGS_HANDLE, flags);			
		}
	}
#endif
}

void scan_transmitter_keys(void)
{
#ifdef TRANSMIT_MODE
	// 10ms period
	// default key state = inactive
	static uint8_t flags;
	// check for level changes (key presses, glitches etc.)
	// filter out glitches
	keypress_filter(GPIO_ReadInputPin(SW_WALK), &sw_walk_key);
	keypress_filter(GPIO_ReadInputPin(SW_GO), &sw_go_key);
	keypress_filter(GPIO_ReadInputPin(SW_STOP), &sw_stop_key);
	keypress_filter(GPIO_ReadInputPin(SW_BLINK), &sw_blink_key);
	keypress_filter(GPIO_ReadInputPin(SW_AUTO), &sw_auto_key);

	flags = scheduler_getVar(FLAGS_HANDLE);	
	// process event based actions
	if (KEY_ACTIVATED == sw_walk_key.event)
	{
		//if (sw_walk_key.level_stable_tick_counter < T_FUNCTION1_MAX)
		//{
			// add function 1 code
			flags |= WALK_FLAG;
			scheduler_setVar(FLAGS_HANDLE, flags);
		//}
		sw_walk_key.event = NO_EVENT;
	}
	
	if (KEY_ACTIVATED == sw_go_key.event)
	{
		//if (sw_go_key.level_stable_tick_counter < T_FUNCTION1_MAX)
		//{
			// add function 1 code
			flags |= GO_FLAG;
			scheduler_setVar(FLAGS_HANDLE, flags);
		//}
		sw_go_key.event = NO_EVENT;
	}	
	
	if (KEY_ACTIVATED == sw_stop_key.event)
	{
		//if (sw_stop_key.level_stable_tick_counter < T_FUNCTION1_MAX)
		//{
			// add function 1 code
			flags |= STOP_FLAG;
			scheduler_setVar(FLAGS_HANDLE, flags);
		//}
		sw_stop_key.event = NO_EVENT;
	}	
	
	if (KEY_ACTIVATED == sw_blink_key.event)
	{
		//if (sw_blink_key.level_stable_tick_counter < T_FUNCTION1_MAX)
		//{
			// add function 1 code
			flags |= BLINK_FLAG;
			scheduler_setVar(FLAGS_HANDLE, flags);
		//}
		sw_blink_key.event = NO_EVENT;
	}	
	
	if (KEY_ACTIVATED == sw_auto_key.event)
	{
		//if (sw_auto_key.level_stable_tick_counter < T_FUNCTION1_MAX)
		//{
			// add function 1 code
			flags |= AUTO_FLAG;
			scheduler_setVar(FLAGS_HANDLE, flags);
		//}
		sw_auto_key.event = NO_EVENT;
	}
	// process time based actions
#endif	
}