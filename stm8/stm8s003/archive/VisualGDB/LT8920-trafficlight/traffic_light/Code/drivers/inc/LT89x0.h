/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LT89X0_H
#define __LT89X0_H

#include "stm8s.h"
#include "stm8s_gpio.h"
#include "spi.h"

extern uint8_t Status, RegH, RegL;

//#define SECOND_LT8920

//LT8920 Chip Enable (nRESET) pin definitions
#define CE	 	GPIOA,GPIO_PIN_1 	/*PA1*/ 
//LT8920 PKT interrupt pin definitions
#define PKT 	GPIOA,GPIO_PIN_2 	/*PA2*/
//LT8920 NSS pin definition
#define NSS	 	GPIOA,GPIO_PIN_3 	/*PA3*/ 

#ifdef SECOND_LT8920
//LT8920 Chip Enable (nRESET) pin definitions
#define CE2	 	GPIOB,GPIO_PIN_4 	/*SCL (pullup resistor required)*/
#define PKT2 	GPIOB,GPIO_PIN_5 	/*SDA (pullup resistor required)*/ 
#define NSS2 	GPIOD,GPIO_PIN_5 	/*PD5*/ 
#endif

//----------------------------------------------------------------------------
void lt89x0_ReadReg(uint8_t reg, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_WriteReg(uint8_t reg, uint8_t byteH, uint8_t byteL, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_TransmitByte(uint8_t Byte, uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_EnableReceiveMode(uint8_t channel, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_WriteConfig(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void lt89x0_Init(void);

#endif

