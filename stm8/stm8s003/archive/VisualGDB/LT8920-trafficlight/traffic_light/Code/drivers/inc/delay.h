/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DELAY_H
#define __DELAY_H

void DelayMs (uint16_t ms);
Delay10us(void);

#endif