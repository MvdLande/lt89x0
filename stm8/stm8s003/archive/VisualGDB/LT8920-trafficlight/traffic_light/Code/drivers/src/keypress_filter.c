#include "keypress_filter.h"
//#include "scheduler\scheduler.h"


#define MIN_KEY_TICKS 5

void keypress_filter_init(keypress_filter_state_t *key, bool invert_input)
{
		key->level_changed_tick_counter = 0;
		key->output = 0;
		key->event = NO_EVENT;
		key->invert_input = (uint8_t) invert_input;
}

void keypress_filter (uint8_t input, keypress_filter_state_t *key)
{
	// If the function is used to filter the level of a push button switch connected to
  // an input pin, then usually the input is configured with a pull-up resistor
  // and one side of the switch is connected to	ground and the other side to the input pin.
  // In this case the switch is "active level = logical low" 
	// therefore the logical input level needs to be inverted first.
	
	uint8_t input_level;
	uint8_t output_level;
	if (key->invert_input != 0)
	{
		// invert input level
		// be aware of the following: the variable "input" can be zero or non-zero, not only
		// zero or one. Therefore logical bit inversion cannot be used.
		if( 0 == input) // (input == zero)
		{
			input_level = 1;
		}
		else
		{
			input_level = 0;
		}
	}
	else
	{
		if( 0 == input)
		{
			input_level = 0;
		}
		else
		{
			input_level = 1;
		}
	}
	output_level = key->output;
	// check if input == output (both logical low) or (both logical high)
	if (input_level == output_level)
	{
		// no level change, clear counter
		key->level_changed_tick_counter = 0;
		if(MAX_KEY_TICKS < key->level_stable_tick_counter)
		{
			key->level_stable_tick_counter = key->level_stable_tick_counter +1;
		}		
	}
	else
	{
		// at this point there is a level change at the input
		// input != output
		key->level_changed_tick_counter = key->level_changed_tick_counter +1; 
		if (key->level_changed_tick_counter > MIN_KEY_TICKS)
		{
			key->level_changed_tick_counter = 0;
			key->level_stable_tick_counter = MIN_KEY_TICKS + 1;
			// input level is stable --> invert output level and
			// set the corresponding level change events
			if ( 0 == key->output)
			{
				key->output = 1;
				key->event = KEY_ACTIVATED; // set level change event type
			}		
			else
			{
				key->output = 0;
				key->event = KEY_RELEASED;
			}
		}				
	}
}