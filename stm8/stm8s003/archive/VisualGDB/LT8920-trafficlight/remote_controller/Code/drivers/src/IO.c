#include "IO.h"

void IO_Init(void)
{
	GPIO_Init(CAR_R, GPIO_MODE_OUT_PP_LOW_SLOW);
	GPIO_Init(CAR_Y, GPIO_MODE_OUT_PP_LOW_SLOW);
	GPIO_Init(CAR_G, GPIO_MODE_OUT_PP_LOW_SLOW);

	GPIO_Init(PED_R, GPIO_MODE_OUT_PP_LOW_SLOW);
	GPIO_Init(PED_G, GPIO_MODE_OUT_PP_LOW_SLOW);
	
	GPIO_Init(SW_WALK,  GPIO_MODE_IN_PU_NO_IT);	
/*	
	GPIO_WriteHigh(CAR_R);	
	GPIO_WriteHigh(CAR_Y);	
	GPIO_WriteHigh(CAR_G);	
	GPIO_WriteHigh(PED_R);
	GPIO_WriteHigh(PED_G);


	GPIO_WriteLow(CAR_R);	
	GPIO_WriteLow(CAR_Y);	
	GPIO_WriteLow(CAR_G);	
	GPIO_WriteLow(PED_R);
	GPIO_WriteLow(PED_G);
*/
}