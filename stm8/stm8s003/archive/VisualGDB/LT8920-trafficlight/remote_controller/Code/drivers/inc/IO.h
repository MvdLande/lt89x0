/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __IO_H
#define __IO_H

#include "stm8s.h"
#include "stm8s_gpio.h"

#define CAR_G 			GPIOC,GPIO_PIN_3 	/*PC3*/
#define CAR_Y 			GPIOC,GPIO_PIN_4 	/*PC4*/
#define CAR_R 			GPIOD,GPIO_PIN_2 	/*PD2*/
#define PED_G 			GPIOD,GPIO_PIN_3 	/*PD3*/
#define PED_R 			GPIOD,GPIO_PIN_5 	/*PD5*/
#define SW_WALK_pin GPIO_PIN_6
#define SW_WALK 		GPIOD,SW_WALK_pin 	/*PD6*/

void IO_Init(void);

#endif