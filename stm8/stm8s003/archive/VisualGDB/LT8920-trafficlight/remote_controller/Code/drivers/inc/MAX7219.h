/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAX7219_H
#define __MAX7219_H

#include "spi.h"

#define MAX7219_Noop					0x0
#define MAX7219_Digit0				0x1
#define MAX7219_Digit1				0x2
#define MAX7219_Digit2				0x3
#define MAX7219_Digit3				0x4
#define MAX7219_Digit4				0x5
#define MAX7219_Digit5				0x6
#define MAX7219_Digit6				0x7
#define	MAX7219_Digit7				0x8
#define MAX7219_DecodeMode		0x9
#define MAX7219_Intensity 		0xA
#define	MAX7219_ScanLimit			0xB
#define	MAX7219_Shutdown			0xC
#define	MAX7219_DisplayTest		0xF		
#define MAX7219_CODEB_NEGATIVE 0xA 
#define MAX7219_CODEB_E     	0xB 
#define MAX7219_CODEB_H     	0xC 
#define MAX7219_CODEB_L     	0xD 
#define MAX7219_CODEB_P     	0xE 
#define MAX7219_CODEB_BLANK   0xF

#define SEG_DP								0x80
#define SEG_A									0x40
#define SEG_B									0x20
#define SEG_C									0x10
#define SEG_D									0x08
#define SEG_E									0x04
#define SEG_F									0x02
#define SEG_G									0x01
#define SEG_OFF								0x00

#define SEG_CHAR_r						(SEG_E + SEG_G)
#define SEG_CHAR_o						(SEG_C + SEG_D + SEG_E + SEG_G)
#define SEG_CHAR_-						(SEG_G)
#define SEG_CHAR_S						(SEG_A + SEG_C + SEG_D + SEG_F + SEG_G)
#define SEG_CHAR_i						(SEG_C)
#define SEG_CHAR_n						(SEG_C + SEG_E + SEG_G)


extern const uint8_t HEX_To_7SEG[];

void max7219_write(uint8_t address, uint8_t data, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void max7219_CodeB_init(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );
void max7219_init(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin );

#endif