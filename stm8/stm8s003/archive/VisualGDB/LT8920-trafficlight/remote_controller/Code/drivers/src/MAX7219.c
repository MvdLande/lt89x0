#include "MAX7219.h"
#include "delay.h"
#include "scheduler.h"

const uint8_t HEX_To_7SEG[] =
{
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_E + SEG_F 				/* 0 */,
	SEG_B + SEG_C 																						/* 1 */,
	SEG_A + SEG_B + SEG_D + SEG_E + SEG_G 									/* 2 */,
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_G 									/* 3 */,
	SEG_B + SEG_C + SEG_F + SEG_G 													/* 4 */,
	SEG_A + SEG_C + SEG_D + SEG_F + SEG_G 									/* 5 */,
	SEG_A + SEG_C + SEG_D + SEG_E + SEG_F + SEG_G 				/* 6 */,
	SEG_A + SEG_B + SEG_C 																		/* 7 */,
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_E + SEG_F + SEG_G /* 8 */,
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_F + SEG_G 				/* 9 */,	
	SEG_A + SEG_B + SEG_C + SEG_E + SEG_F + SEG_G 				/* A */,
	SEG_C + SEG_D + SEG_E + SEG_F + SEG_G 									/* b */,
	SEG_A + SEG_D + SEG_E + SEG_F 													/* C */,
	SEG_B + SEG_C + SEG_D + SEG_E + SEG_G 				/* d */,
	SEG_A + SEG_D + SEG_E + SEG_F + SEG_G 									/* E */,
	SEG_A + SEG_E + SEG_F + SEG_G 													/* F */
};


void max7219_write(uint8_t address, uint8_t data, GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin ){

	scheduler_enterCriticalSection();
	GPIO_WriteLow(SPI_NSS_port, SPI_NSS_pin);
	spi_ReadWrite(0x0F & address);
  spi_ReadWrite(data);
	GPIO_WriteHigh(SPI_NSS_port, SPI_NSS_pin);
	scheduler_leaveCriticalSection();
}

void max7219_CodeB_init(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin ){
	DelayMs(500);	
	max7219_write(MAX7219_Shutdown, 0x00, SPI_NSS_port, SPI_NSS_pin); // disable display
	max7219_write(MAX7219_DecodeMode, 0xFF, SPI_NSS_port, SPI_NSS_pin); // BCD mode for all digits
	max7219_write(MAX7219_ScanLimit, 0x07, SPI_NSS_port, SPI_NSS_pin); // enable all 8 digits
	//max7219_write(ScanLimit, 0x00, SPI_NSS_port, SPI_NSS_pin); // enable 1 digit
	max7219_write(MAX7219_Intensity, 0x04, SPI_NSS_port, SPI_NSS_pin); // set intensity level 
	max7219_write(MAX7219_Digit0, MAX7219_CODEB_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit1, MAX7219_CODEB_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit2, MAX7219_CODEB_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit3, MAX7219_CODEB_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit4, MAX7219_CODEB_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit5, MAX7219_CODEB_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit6, MAX7219_CODEB_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit7, MAX7219_CODEB_NEGATIVE, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Shutdown, 0x01, SPI_NSS_port, SPI_NSS_pin); // enable display
}

void max7219_init(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin ){
	DelayMs(500);	
	max7219_write(MAX7219_Shutdown, 0x00, SPI_NSS_port, SPI_NSS_pin); // disable display
	max7219_write(MAX7219_DecodeMode, 0x00, SPI_NSS_port, SPI_NSS_pin); // direct mode for all digits
	max7219_write(MAX7219_ScanLimit, 0x07, SPI_NSS_port, SPI_NSS_pin); // enable all 8 digits
	//max7219_write(ScanLimit, 0x00, SPI_NSS_port, SPI_NSS_pin); // enable 1 digit
	max7219_write(MAX7219_Intensity, 0x04, SPI_NSS_port, SPI_NSS_pin); // set intensity level 
	max7219_write(MAX7219_Digit0, SEG_OFF, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit1, SEG_OFF, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit2, SEG_OFF, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit3, SEG_OFF, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit4, SEG_OFF, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit5, SEG_OFF, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit6, SEG_OFF, SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Digit7, HEX_To_7SEG[0], SPI_NSS_port, SPI_NSS_pin); 
	max7219_write(MAX7219_Shutdown, 0x01, SPI_NSS_port, SPI_NSS_pin); // enable display
}

