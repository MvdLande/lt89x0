#include "stm8s.h"
#include "stm8s_gpio.h"
#include "stm8s_spi.h"
#include "stm8s_tim4.h"
#include "spi.h"
#include "lt89x0.h"
#include "max7219.h"
#include "stm8s_clk.h"
#include "io.h"
#include "KeyscanTask.h"
#include "CommunicationTask.h"
#include "statemachineTask.h"
#include "common.h"



void DelayMs (uint16_t ms){ //Function Definition
  uint16_t i =0 ;
  uint8_t j=0;
  for (i=0; i<=ms; i++)
  {
    for (j=0; j<120; j++) // Nop = Fosc/4
      _asm("nop"); //Perform no operation //assembly code              
  }
}

Delay10us(void)
{
	uint8_t j=0;
	for (j=0; j<12; j++) // Nop = Fosc/4
		_asm("nop"); //Perform no operation //assembly code              
}



void main(){
	static uint32_t i=0, j=0;
	spi_Init();
	lt89x0_Init();
	lt89x0_EnableReceiveMode(channel, LT8920);
	IO_Init();	
	// init shared variables
	scheduler_setVar(FLAGS_HANDLE, 0x00);
	scheduler_setVar(INIT_HANDLE, INIT);
	
	init_keys();

	scheduler_initTasks();
	// prio 0 has the highest priority
	scheduler_createTask (&keyscan_task, PRIO0, 2, 0, TASK_PERIODIC);	// run task every 2 systicks
	scheduler_createTask (&statemachine_task, PRIO1, 1, 0, TASK_PERIODIC);	// run task every 1 systicks
	scheduler_createTask (&communication_task, PRIO2, 1, 0, TASK_PERIODIC);	// run task every 1 systicks
	// not used //CreateTask (&DisplayTask, PRIO3, 1, 0, TASK_PERIODIC);	// run task every 1 systicks
	
	// Task number equals task priority
	scheduler_enableTask (PRIO0);
	scheduler_enableTask (PRIO1);	
	scheduler_enableTask (PRIO2);		
	//not used //scheduler_enableTask (PRIO3);		
	scheduler_start();

	
  while(1)
  {
	  wfi(); 
	  /*
		i++;
		if(i%1000 == 0)
		{
			j++;
			if (j%10 == 0)
			{
				j = j;
			}
		}
		//DelayMs(500);
		*/
	}
}


#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif