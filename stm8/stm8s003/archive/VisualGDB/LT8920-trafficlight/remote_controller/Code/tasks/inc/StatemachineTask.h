/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STATEMACHINETASK_H
#define __STATEMACHINETASK_H

void statemachine_task(void);

#endif