#include "stm8s.h"
#include "stm8s_gpio.h"
#include "stm8s_spi.h"
#include "stm8s_i2c.h"
#include "spi.h"
#include "pin_definitions.h"



void DelayMs (uint16_t ms){ //Function Definition
  uint16_t i =0 ;
  uint8_t j=0;
  for (i=0; i<=ms; i++)
  {
    for (j=0; j<120; j++) // Nop = Fosc/4
      _asm("nop"); //Perform no operation //assembly code              
  }
}

Delay10us(void)
{
	uint8_t j=0;
	for (j=0; j<12; j++) // Nop = Fosc/4
		_asm("nop"); //Perform no operation //assembly code              
}

void initMAX7219(GPIO_TypeDef* SPI_NSS_port, GPIO_Pin_TypeDef SPI_NSS_pin ){
	DelayMs(500);	
	spiWriteMAX7219(Shutdown, 0x00, SPI_NSS_port, SPI_NSS_pin); // disable display
	spiWriteMAX7219(DecodeMode, 0xFF, SPI_NSS_port, SPI_NSS_pin); // BCD mode for all digits
	spiWriteMAX7219(ScanLimit, 0x07, SPI_NSS_port, SPI_NSS_pin); // enable all 8 digits
	//spiWriteMAX7219(ScanLimit, 0x00, SPI_NSS_port, SPI_NSS_pin); // enable 1 digit
	spiWriteMAX7219(Intensity, 0x04, SPI_NSS_port, SPI_NSS_pin); // set intensity level 
	spiWriteMAX7219(Digit0, MAX7219_CHAR_H, SPI_NSS_port, SPI_NSS_pin); 
	spiWriteMAX7219(Digit1, MAX7219_CHAR_E, SPI_NSS_port, SPI_NSS_pin); 
	spiWriteMAX7219(Digit2, MAX7219_CHAR_L, SPI_NSS_port, SPI_NSS_pin); 
	spiWriteMAX7219(Digit3, MAX7219_CHAR_P, SPI_NSS_port, SPI_NSS_pin); 
	spiWriteMAX7219(Digit4, MAX7219_CHAR_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	spiWriteMAX7219(Digit5, MAX7219_CHAR_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	spiWriteMAX7219(Digit6, MAX7219_CHAR_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	spiWriteMAX7219(Digit7, MAX7219_CHAR_BLANK, SPI_NSS_port, SPI_NSS_pin); 
	spiWriteMAX7219(Shutdown, 0x01, SPI_NSS_port, SPI_NSS_pin); // enable display
}


void main(){
	uint8_t status, channel;
	uint8_t i=0,j=0;
	channel = 100;
	InitSPI();
	//initMAX7219(NSS);
	initMAX7219(NSS);	
	//InitLT8900();
	//spiReadreg(7, NSS);
	//spiReadreg(7, NSS2);
	//spiEnableReceiveMode(channel,NSS2);
	//spiWriteKey(0xe0, channel, NSS);
	//spiReadreg(48, NSS2); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
	//spiReadreg(50, NSS2); //read received data


	//while (GPIO_ReadInputPin(PKT_LT8920_extra)); //wait if PKT is high (it should be low)  
	// transmit a test packet of one byte 
		
	
	//BitStatus GPIO_ReadInputPin(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin)

	
	//Declare PA2  as input pull up pin
	//GPIO_Init (GPIOA, GPIO_PIN_2, GPIO_MODE_IN_PU_IT);

	//Declare PB5 as push pull Output pin
  //GPIO_Init (GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

  //GPIO_Init (GPIOC, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);

	//GPIO_WriteHigh(GPIOC, GPIO_PIN_3); //LED ON
	
  while(1){
		if(i>100)
		{
			spiWriteMAX7219(Digit0, MAX7219_CHAR_BLANK, NSS); 
			spiWriteMAX7219(Digit1, MAX7219_CHAR_BLANK, NSS); 			
			spiWriteMAX7219(Digit2, MAX7219_CHAR_BLANK, NSS); 
			spiWriteMAX7219(Digit3, MAX7219_CHAR_BLANK, NSS); 			
			DelayMs(2000);
			spiWriteMAX7219(Digit0, MAX7219_CHAR_H, NSS); 
			spiWriteMAX7219(Digit1, MAX7219_CHAR_E, NSS); 			
			spiWriteMAX7219(Digit2, MAX7219_CHAR_L, NSS); 
			spiWriteMAX7219(Digit3, MAX7219_CHAR_P, NSS); 			
			DelayMs(2000);
			spiWriteMAX7219(Digit0, MAX7219_CHAR_BLANK, NSS); 
			spiWriteMAX7219(Digit1, MAX7219_CHAR_BLANK, NSS); 			
			spiWriteMAX7219(Digit2, MAX7219_CHAR_BLANK, NSS); 
			spiWriteMAX7219(Digit3, MAX7219_CHAR_BLANK, NSS); 			
			DelayMs(2000);
			spiWriteMAX7219(Digit0, MAX7219_CHAR_H, NSS); 
			spiWriteMAX7219(Digit1, MAX7219_CHAR_E, NSS); 			
			spiWriteMAX7219(Digit2, MAX7219_CHAR_L, NSS); 
			spiWriteMAX7219(Digit3, MAX7219_CHAR_P, NSS); 			
			DelayMs(2000);
			i=0;
		}

		//GPIO_WriteReverse(GPIOB,GPIO_PIN_5);
		//GPIO_WriteLow(GPIOC, GPIO_PIN_3); //LED OFF
		//spiEnableReceiveMode(channel, NSS2);
		DelayMs(500);
		//GPIO_WriteHigh(GPIOC, GPIO_PIN_3); //LED ON

		//spiWriteKey(0xe0, channel , NSS);
		DelayMs(20);	
		//spiReadreg(48, NSS2); //read status register	(bit 15 = CRC ststus (1=CRC ERROR))
		//spiReadreg(52, NSS2); //read fifo status register		(id auto-ack=on then, if bits 0-5 are zero then the ack sequence was completed corretly
		//spiReadreg(50, NSS2); //read received data (first byte = number of byte received (RegH))	
		DelayMs(480);
		if(i<10)
		{
			spiWriteMAX7219(Digit7, i, NSS); 
			spiWriteMAX7219(Digit6, MAX7219_CHAR_BLANK, NSS); 
			spiWriteMAX7219(Digit5, MAX7219_CHAR_BLANK, NSS); 
		}	
		else if (i<100)
		{
			j = i%10;
			spiWriteMAX7219(Digit7, j, NSS); 
			j = i/10;
			spiWriteMAX7219(Digit6, j, NSS); 
			spiWriteMAX7219(Digit5, MAX7219_CHAR_BLANK, NSS); 
		}
		else
		{
			spiWriteMAX7219(Digit7, 0, NSS); 
			spiWriteMAX7219(Digit6, 0, NSS); 
			spiWriteMAX7219(Digit5, 1, NSS); 
		}
		i++;
	}
}


#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif